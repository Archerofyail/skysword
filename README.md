SkySword
========

A Hack 'n' Slash, open world game made in Unity For practice. 

Features
========

* The player is able to walk around, jump, and can fly by jumping and pressing shift in the air
* There are a number of menus
  * An inventory menu opened with I
  * A map menu opened with M
  * A quest journal opened with Tab
  * A skills menu opened with N
  * A remote purchase menu opened with R
* There are also NPCs the player is able to talk to
  * Quest givers which, as you might expect, give you quests to complete
  * and Shop Keepers, who allow you to purchase things from their shop.
* The player is able to equip things from the inventory menu, and armour will show up when equipped
* There are tooltips that can appear when triggered, and can optionally pause the game while displaying
