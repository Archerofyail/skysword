﻿using System.Linq.Expressions;
using UnityEngine;
using System.Collections;

public class IntroGUI : MonoBehaviour
{
	public GUIStyle GUIStyle;
	public bool isShowing = true;
	
	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.H))
		{
			isShowing = true;
		}
		if (isShowing)
		{
			Time.timeScale = 0;
			Cursor.visible = true;
			Cursor.lockState = CursorLockMode.Confined;
		}
		else
		{
			Time.timeScale = 1;
			if (Cursor.lockState != CursorLockMode.Locked)
			{
				Cursor.lockState = CursorLockMode.Locked;
			}
		}
	}

	void OnGUI()
	{
		if (isShowing)
		{
			GUI.Box(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 113, 400, 285),
				"Ground Controls:\n" + 
				"Move with wasd and control the\n" + 
				"camera with the mouse.\n" +
				"Do regular attacks with left mouse click.\n" + 
				"Jump in the air and press right mouse button to \n" +
				"do a dive attack at the ground.(2 sec cooldown)\n" + 
				"Jump in the air and press shift to start flying.\n" +
				"Air Controls:\n" + 
				"move with wasd and control the \n" + 
				"camera with the mouse." +
				"Press and hold shift to go faster.\n" + 
				"Press q to do a short dash that can knock out\n" +
				"flying enemies for a short time (2 sec cooldown)\n" +
				"Press H at any time to bring this up again\n" +
				"P.S.: I would highly recommend playing the build of this which is\n" +
				" in the Build folder at the root of this project's folder");
			if (GUI.Button(new Rect(Screen.width / 2 - 75, Screen.height / 2 + 125, 150, 25), "Got it"))
			{
				isShowing = false;
				Destroy(this, 1f);
			}
		}
	}
}
