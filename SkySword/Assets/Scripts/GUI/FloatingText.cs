﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class FloatingText : MonoBehaviour
{
	private Camera playerCam;
	public List<ShopKeeper> shopKeepers;
	public List<QuestGiver> questGivers; 
	public float textAppearDist = 10;
	void Start()
	{
		playerCam = GetComponent<Camera>();
		shopKeepers = new List<ShopKeeper>();
		shopKeepers.AddRange(FindObjectsOfType<ShopKeeper>());
		questGivers = new List<QuestGiver>();
		questGivers.AddRange(FindObjectsOfType<QuestGiver>());
	}

	void OnGUI()
	{
		if (!MenuManager.isAMenuEnabled)
		{
			foreach (var shopKeeper in shopKeepers)
			{
				if (shopKeeper.GetComponent<Renderer>().isVisible)
				{
					var screenPos = playerCam.WorldToScreenPoint(shopKeeper.transform.position + Vector3.up*2.2f);

					if (screenPos.z >= 0 && Vector3.Distance(transform.position, shopKeeper.transform.position) < textAppearDist)
					{
						GUI.Label(new Rect(screenPos.x - 25, Screen.height - screenPos.y, 200, 200), shopKeeper.ShopName);
					}
				}
			}
			foreach (var questGiver in questGivers)
			{
				if (questGiver.GetComponent<Renderer>().isVisible)
				{
					var screenPos = playerCam.WorldToScreenPoint(questGiver.transform.position + Vector3.up*2.2f);

					if (screenPos.z >= 0 && Vector3.Distance(transform.position, questGiver.transform.position) < textAppearDist)
					{
						GUI.Label(new Rect(screenPos.x - 25, Screen.height - screenPos.y, 200, 200), questGiver.Name);
					}
				}
			}
		}
	}
}
