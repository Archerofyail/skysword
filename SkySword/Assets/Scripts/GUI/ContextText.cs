﻿using UnityEngine;

public class ContextText : MonoBehaviour
{
	public float RayDistance = 3f;
	public bool hitItem;
	public string conText;
	public LayerMask layerMask = 11;
	public string targetTag;
	public RaycastHit rayInfo;

	void Update()
	{

		if (Physics.SphereCast(transform.position, 2f, transform.forward, out rayInfo, RayDistance, layerMask.value))
		{
			targetTag = rayInfo.transform.tag;
			if (targetTag == Tags.Pickup)
			{
				hitItem = true;
				conText = "Press E to Pickup " + rayInfo.transform.GetComponent<Pickup>().Item.ItemName;
			}
			else if (targetTag == Tags.ShopKeep)
			{
				conText = "Press E to shop";
				hitItem = true;
			}
			else if (targetTag == Tags.QuestGiver)
			{
				conText = "Press E to view quest";
			}

		}
		else
		{
			hitItem = false;
			conText = "";
		}

	}

	private void OnGUI()
	{
		if (!MenuManager.isAMenuEnabled)
		{
			GUI.TextArea(new Rect(Screen.width / 1.35f, Screen.height / 1.65f, Screen.width / 7, Screen.height / 15), conText);
		}
	}
}
