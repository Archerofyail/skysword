﻿using UnityEngine;
using System.Collections;

public class InfoToolTip : MonoBehaviour
{
	public bool shouldFreezeControl;
	public bool shouldBeDestroyed;
	private bool isActive;
	public float timeActiveMax = 2f;
	private float timeActive;
	public string toolTip = "Insert Tooltip here";
	private bool hasActivated;

	public delegate void ToolTipClosedEvent();

	public event ToolTipClosedEvent ToolTipClosedEventHandler;

	void OnDestroy()
	{

	}

	void Update()
	{
		if (isActive && !shouldFreezeControl)
		{
			timeActive += Time.deltaTime;
			if (timeActive > timeActiveMax)
			{
				isActive = false;
				timeActive = 0;
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
		{
			if (!hasActivated)
			{
				if (shouldFreezeControl)
				{
					Cursor.lockState = CursorLockMode.None;
					Cursor.visible = true;
					PlayerControlHandler.DisablePlayerControl();
				}
				isActive = true;
				hasActivated = true;
			}
		}
	}

	void OnTriggerStay(Collider other)
	{
		if (other.tag == "Player")
		{
			if (!hasActivated)
			{
				if (shouldFreezeControl)
				{
					Cursor.lockState = CursorLockMode.None;
					Cursor.visible = true;
					PlayerControlHandler.DisablePlayerControl();
				}
				isActive = true;
				hasActivated = true;
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player")
		{
			hasActivated = false;
		}
	}

	void OnGUI()
	{
		if (isActive)
		{
			GUILayout.BeginArea(new Rect(Screen.width * 0.17f, Screen.height * 0.1f, Screen.width * 0.35f, Screen.height * 0.23f), GUI.skin.FindStyle("Box"));
			GUILayout.Label(toolTip);
			if (shouldFreezeControl)
			{
				Time.timeScale = 0.01f;
				Time.fixedDeltaTime = 0.01f * 0.01f;
				if (GUILayout.Button("OK"))
				{
					Time.timeScale = 1;
					Time.fixedDeltaTime = 0.02f;
					isActive = false;
					Cursor.lockState = CursorLockMode.Locked;
					Cursor.visible = false;
					PlayerControlHandler.EnablePlayerControl();
					if (shouldBeDestroyed)
					{
						Destroy(gameObject);
					}
					if (ToolTipClosedEventHandler != null)
					{
						ToolTipClosedEventHandler();
					}
				}
			}
			GUILayout.EndArea();
		}
	}
}
