﻿using System.Collections.Generic;
using UnityEngine;

public class DamageText : MonoBehaviour
{
	public List<DamageTextContainer> damageTexts;
	public List<DamageTextContainer> destroyedCounter = new List<DamageTextContainer>();
	private bool isActive;
	private float activeTimerMax = 3f;
	public GUISkin skinToUse;
	
	void Start()
	{
		damageTexts = new List<DamageTextContainer>();
	}
	
	void Update()
	{
		
		foreach (var dText in damageTexts)
		{
			dText.timer += Time.deltaTime;
			if (dText.timer > activeTimerMax)
			{
				destroyedCounter.Add(dText);
			}
		}
		foreach (var dText in destroyedCounter)
		{
			damageTexts.Remove(dText);
		}
		destroyedCounter.Clear();
	}

	void OnGUI()
	{
		GUI.skin = skinToUse;
		foreach (var dText in damageTexts)
		{
			var screenPos = Camera.main.WorldToScreenPoint(dText.worldPos);
			if (screenPos.z > 0)
			{
				GUI.color = new Color(1, 1, 1, 1 - (dText.timer / 2));
				GUI.Label(new Rect(screenPos.x, Screen.height - (screenPos.y + (dText.timer * 20)), 80, 75), dText.text, skinToUse.GetStyle("Label"));
				GUI.color = Color.white;
			}
		}
	}

	public void AddText(string text, Vector3 worldPos)
	{
		damageTexts.Add(new DamageTextContainer(text, worldPos));
	}
}

public class DamageTextContainer
{
	public string text;
	public Vector3 worldPos;
	public float timer;

	public DamageTextContainer(string text, Vector3 worldPos)
	{
		this.text = text;
		this.worldPos = worldPos;
		timer = 0;
	}
}
