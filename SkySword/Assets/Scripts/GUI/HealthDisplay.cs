﻿using System;
using UnityEngine;

public class HealthDisplay : MonoBehaviour
{

	// ReSharper disable MemberCanBePrivate.Global
	// ReSharper disable UnassignedField.Global
	public Texture2D healthBarBorder;
	public Texture2D healthBarInner;
	public Texture2D healthBarFill;
	// ReSharper restore UnassignedField.Global
	private Texture2D healthBarFillModHealth;
	private float healthRatio;

	public Rect healthBarBorderRect = new Rect(0.02f, 0.02f, 0.2f, 0.1f);

	public Rect healthBarFillRect = new Rect(0.03f, 0.03f, 0.08f, 0.07f);
	// ReSharper restore MemberCanBePrivate.Global
	private Rect HealthBarBorderRect
	{
		get
		{
			return new Rect(Screen.width * healthBarBorderRect.x, Screen.height * healthBarBorderRect.y,
				Screen.width * healthBarBorderRect.width, Screen.height * healthBarBorderRect.height);
		}
	}

	private Rect HealthBarInnerRect
	{
		get
		{
			return new Rect(Screen.width * healthBarFillRect.x, Screen.height * healthBarFillRect.y,
				Screen.width * healthBarFillRect.width, Screen.height * healthBarFillRect.height);
		}
	}
	private Rect HealthBarFillRect
	{
		get
		{
			return new Rect(Screen.width * healthBarFillRect.x, Screen.height * healthBarFillRect.y,
				(Screen.width * healthBarFillRect.width) * Mathf.Clamp01(PlayerDamageTracker.GetHealthToMaxHealthRatio()), Screen.height * healthBarFillRect.height);
		}
	}

	private Texture2D HealthBarFillTex
	{
		get
		{
			if (Math.Abs(PlayerDamageTracker.GetHealthToMaxHealthRatio() - healthRatio) > 0.001)
			{
				healthRatio = PlayerDamageTracker.GetHealthToMaxHealthRatio();
				var texWidth = healthBarFill.width * PlayerDamageTracker.GetHealthToMaxHealthRatio();
				var texHeight = healthBarFill.height;
				var healthBarTex = new Texture2D((int)texWidth, texHeight);
				var pixelsAcquired = healthBarFill.GetPixels(0, 0, (int)texWidth, texHeight);
				healthBarTex.SetPixels(pixelsAcquired);
				healthBarTex.Apply();
				healthBarFillModHealth = healthBarTex;
				healthBarFillModHealth.Apply();
				return healthBarTex;
			}

			return healthBarFillModHealth;
		}
	}

	private void OnGUI()
	{
		if (!MenuManager.isAMenuEnabled)
		{
			GUI.DrawTexture(HealthBarInnerRect, healthBarInner, ScaleMode.StretchToFill);
			GUI.DrawTexture(HealthBarFillRect, HealthBarFillTex, ScaleMode.StretchToFill);
			GUI.DrawTexture(HealthBarBorderRect, healthBarBorder, ScaleMode.StretchToFill);
		}
	}
}
