﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class CommonEnemyData : MonoBehaviour
{
	public List<GroundEnemy> enemies;
	public Transform playerTransform;
	
	void Start()
	{
		GameObject.Find("Player");
		enemies = new List<GroundEnemy>(16);
		foreach (var enemy in GameObject.FindGameObjectsWithTag("GroundEnemy"))
		{
			enemies.Add(enemy.GetComponent<GroundEnemy>());
		}
		StartCoroutine("UpdateEnemyLocation");
	}
	 
	void Update()
	{

	}

	IEnumerator UpdateEnemyLocation()
	{
		while (playerTransform)
		{
			foreach (var enemy in enemies)
			{
				if (enemy)
				{
					enemy.playerPos = playerTransform.position;
					enemy.enemiesNearPlayer = playerTransform.GetComponent<CommonPlayerData>().NumOfEnemiesNearBy;
				}
			}
			yield return null;
		}
	}
}
