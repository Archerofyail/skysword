﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;
using System.Collections;
[Serializable]
public class GroundEnemy : MonoBehaviour
{
	[SerializeField] private int health = 100;
	[SerializeField] private GameObject[] patrolTargets;
	private int currentPatrolPointTarget;
	private UnityEngine.AI.NavMeshAgent navMeshAgent;
	public Vector3 playerPos;
	[SerializeField]private float viewDistance = 15f;
	public float enemiesNearPlayer;
	void Start()
	{
		navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
		if (patrolTargets.Length > 0)
		{
			navMeshAgent.SetDestination(patrolTargets[0].transform.position);
			StartCoroutine("PatrolRoute");
		}
	}

	void Update()
	{
		if (health > 0)
		{
			if (Vector3.Distance(transform.position, playerPos) < viewDistance && enemiesNearPlayer < 2)
			{
				StopCoroutine("PatrolRoute");
				navMeshAgent.stoppingDistance = 3f;
				navMeshAgent.SetDestination(playerPos);
			}
			else
			{
				navMeshAgent.isStopped = true ;
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == Tags.PlayerSword)
		{
			if (other.transform.root.GetComponent<WeaponContainer>().isAttacking)
			{
				if (GetComponent<Animation>().isPlaying)
				{
					GetComponent<Animation>().Stop();
				}
				GetComponent<Animation>().Play("ReelBack");
				if (other.transform.root.GetComponent<Animation>()["LeftToRightSwordSwing"].enabled || other.transform.root.GetComponent<Animation>()["LeftToRightSlash"].enabled)
				{
					ApplyDamage(20, other.transform.parent.right);
				}
				else if (other.transform.root.GetComponent<Animation>()["RightToLeftSwordSwing"].enabled)
				{
					ApplyDamage(20, -other.transform.parent.right);
				}
				else
				{
					ApplyDamage(20, -transform.forward);
				}
			}
		}
	}

	IEnumerator PatrolRoute()
	{
		while (true)
		{
			navMeshAgent.stoppingDistance = 0;

			if (patrolTargets[currentPatrolPointTarget].transform.position.x == transform.position.x && patrolTargets[currentPatrolPointTarget].transform.position.z == transform.position.z)
			{
				currentPatrolPointTarget++;
				if (currentPatrolPointTarget >= patrolTargets.Length)
				{
					currentPatrolPointTarget = 0;
				}
			}
			else
			{
				navMeshAgent.SetDestination(patrolTargets[currentPatrolPointTarget].transform.position);
			}

			yield return null;
		}
	}

	void ApplyDamage(int damageToApply, Vector3 forceDirection)
	{
		health -= damageToApply;
		if (health <= 0)
		{
			Destroy(gameObject, 2f);
			if (!GetComponent<Rigidbody>())
			{
				gameObject.AddComponent<Rigidbody>();
			}
			Destroy(navMeshAgent);
			GetComponent<Rigidbody>().AddForceAtPosition(forceDirection * 500, transform.position + (Vector3.up * 1.5f));
			GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
		}
	}
}
