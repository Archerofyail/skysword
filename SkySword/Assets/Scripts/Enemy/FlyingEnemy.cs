﻿using System;
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[Serializable]
public class FlyingEnemy : MonoBehaviour
{
	[SerializeField]
	private int
			health = 100;
	[SerializeField]
	private GameObject[]
			patrolTargets;
	private int currentPatrolPointTarget;
	public Transform playerPos;
	[SerializeField]
	private float
			viewDistance = 15f;
	public float enemiesNearPlayer;
	private bool isKnockedOut;
	
	void Start()
	{
		playerPos = GameObject.FindGameObjectWithTag("Player").transform.root.transform;
	}

	
	void Update()
	{
		//If you're speed is less than 0.7f
		if (Mathf.Abs(GetComponent<Rigidbody>().velocity.magnitude) < 0.7f && !isKnockedOut)
		{
			//And you're not already playing the animation
			if (!GetComponent<Animation>()["FlyingHover"].enabled)
			{
				//Play it
				GetComponent<Animation>().Play("FlyingHover");
			}
		}
		//otherwise if you are going faster than .7f and the animation is playing
		else if (GetComponent<Animation>()["FlyingHover"].enabled)
		{
			//stop it
			GetComponent<Animation>().Stop("FlyingHover");
		}
		var drag = Mathf.Log(GetComponent<Rigidbody>().velocity.magnitude, 3);
		GetComponent<Rigidbody>().drag = drag < 0 ? 0 : drag;
		if (health > 0 && !isKnockedOut)
		{
			var distFromPlayer = Vector3.Distance(transform.position, playerPos.position);
			if (distFromPlayer < viewDistance && distFromPlayer > 1.5f && enemiesNearPlayer < 2)
			{
				transform.LookAt(playerPos);
				GetComponent<Rigidbody>().AddRelativeForce(0, 0, 25);
			}
			if (distFromPlayer <= 2)
			{
				GetComponent<Rigidbody>().velocity = Vector3.zero;
			}
		}
	}

	void ApplyDamage(int damageToApply, Vector3 forceDirection)
	{
		health -= damageToApply;
		if (health <= 0)
		{
			Destroy(gameObject, 2f);
			if (!GetComponent<Rigidbody>())
			{
				gameObject.AddComponent<Rigidbody>();
			}
			GetComponent<Rigidbody>().AddForceAtPosition(forceDirection * 500, transform.position + (Vector3.up * 1.5f));
			GetComponent<Rigidbody>().useGravity = true;
			GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
		}
	}

	void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag == "Player")
		{
			if (other.transform.root.gameObject.GetComponent<PlayerFlyingMovement>().isDashAttacking)
			{
				ApplyDamage(20, other.relativeVelocity);
				GetComponent<Rigidbody>().AddRelativeForce(other.relativeVelocity);
				StartCoroutine("KnockedOut");
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "PlayerSword")
		{
			if (other.transform.root.GetComponent<WeaponContainer>().isAttacking)
			{
				if (GetComponent<Animation>().isPlaying)
				{
					GetComponent<Animation>().Stop();
				}
				GetComponent<Animation>().Play("ReelBack");
				if (other.transform.root.GetComponent<Animation>()["LeftToRightSwordSwing"].enabled || other.transform.root.GetComponent<Animation>()["LeftToRightSlash"].enabled)
				{
					ApplyDamage(20, other.transform.parent.right);
				}
				else if (other.transform.root.GetComponent<Animation>()["RightToLeftSwordSwing"].enabled)
				{
					ApplyDamage(20, -other.transform.parent.right);
				}
				else
				{
					ApplyDamage(20, -transform.forward);
				}
			}
		}

	}

	IEnumerator KnockedOut()
	{
		GetComponent<Rigidbody>().angularDrag = 0.05f;
		float timeKnockedOut = 0;
		float timeKnockedOutMax = 1.5f;
		isKnockedOut = true;
		while (timeKnockedOut < timeKnockedOutMax)
		{
			timeKnockedOut += Time.deltaTime;
			GetComponent<Rigidbody>().useGravity = true;
			GetComponent<Rigidbody>().drag = 0;
			yield return null;
		}
		isKnockedOut = false;
		GetComponent<Rigidbody>().useGravity = false;
		GetComponent<Rigidbody>().angularDrag = 0.75f;
	}
}