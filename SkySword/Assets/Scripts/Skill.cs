﻿using System.Collections.Generic;
using UnityEngine;

public enum SkillType
{
	StatBoost,
	Move
}
public class Skill
{
	//The zero-based index of the skill tier
	public int tier;
	public int rank = 0;
	public string skillName;
	public string skillDescription;
	public Texture2D skillTex;
	//Skills needed before this one becomes available
	public List<string> dependentSkills;

	public SkillType skillType;

	public Skill()
	{
		tier = 0;
		rank = 0;
		skillName = "Default Skill Name";
		skillDescription = "Default description";
		dependentSkills = new List<string>();
		skillTex = null;
	}
}
