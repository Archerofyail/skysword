﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

public enum QuestStatus
{
	NotAccepted,
	Accepted,
	Completed
}

/// <summary>
/// The quest object
/// </summary>
[Serializable]
public class Quest
{
	public string questName;
	//Who gives out this quest/who you return to to complete the quest
	public string questGiver;
	public string questDescription;
	public string objectiveDescription { get; private set; }
	public bool isAvailable = true;
	public int skillPointsGained;
	public QuestStatus questStatus = QuestStatus.NotAccepted;
	//The objectives of the quest, if one is not visible, it becomes visible when the one directly behind it is completed
	//(e.g. if the objective at 0 gets completed the objective at 1 becomes visible
	public List<QuestObjective> questObjectives;

	public Quest()
	{
		isAvailable = true;
		skillPointsGained = 1;
		questName = "Default Title";
		questDescription = "Default description";
		questObjectives = new List<QuestObjective>();
		SetQuestObjectivesString();
	}

	public Quest(string questName, string questDescription, int skillPointsGained, List<QuestObjective> questObjectives)
	{
		this.skillPointsGained = skillPointsGained;
		this.questObjectives = questObjectives;
		this.questName = questName;
		this.questDescription = questDescription;
		this.questObjectives = new List<QuestObjective>();
		SetQuestObjectivesString();
	}

	public void SetQuestObjectiveEvents()
	{
		foreach (var objective in questObjectives)
		{
			objective.SetObjectiveEvent();
		}
	}
	/// <summary>
	/// Checks if the quest is completed and sets the quest status to completed if so
	/// </summary>
	/// <returns>whether or not the quest is complete</returns>
	public bool CheckIfCompleted()
	{
		bool isCompleted = questObjectives.All(questObjective => questObjective.isCompleted);
		questStatus = isCompleted ? QuestStatus.Completed : questStatus;
		return isCompleted;
	}

	/// <summary>
	/// Sets the objective description for the quest with the currently visible objectives
	/// </summary>
	public void SetQuestObjectivesString()
	{
		objectiveDescription = "• Skill points gained: " + skillPointsGained;
		foreach (var questObjective in questObjectives)
		{
			if (questObjective.isVisible)
			{
				objectiveDescription += "\n" + questObjective.ObjectiveString();
			}
		}
	}
}

[Serializable]
public class QuestObjective
{
	//Each quest get's the name of it's target(s). It then subscribes to the QuestObjectiveCompleted 
	//event on those targets and gets called automatically when the event triggers.
	//It then removes itself from the event an
	public bool isCompleted;
	public Vector3 worldPositon;
	/// <summary>
	/// The name of the objective callers that this objective tracks
	/// </summary>
	public string targetName;
	/// <summary>
	/// The name of the even that this needs to be considered complete. 
	/// Get them from ObjectiveEventNames or use Unity's button input names if you want to track a key.
	/// </summary>
	public string targetSender;
	public string objectiveDescription;
	public int amountToComplete;
	public int amountCompleted;

	public bool isVisible;

	public string ObjectiveString()
	{
		return objectiveDescription + " - " + amountCompleted + "/" + amountToComplete;
	}

	public QuestObjective()
	{
		isVisible = true;
		isCompleted = false;
		amountToComplete = 1;
		amountCompleted = 0;
		targetName = "Target Objective Caller Name. Use Input Caller for any key inputs";
		targetSender = ObjectiveEventNames.ObjectDestroyed + " | " + ObjectiveEventNames.PlayerTrigger + " | any Unity key input names";
		worldPositon = new Vector3(0, 0, 0);
	}

	public QuestObjective(Vector3 worldPos)
	{
		isVisible = true;
		isCompleted = false;
		worldPositon = worldPos;

	}

	/// <summary>
	/// Finds the object(s) this objective needs, and subscribes to their event caller
	/// </summary>
	public void SetObjectiveEvent()
	{
		var objects = Object.FindObjectsOfType<GameObject>();
		foreach (var o in objects)
		{

			var questObjectiveCaller = o.GetComponent<QuestObjectiveCaller>();
			if (questObjectiveCaller)
			{
				if (questObjectiveCaller.objectiveName == targetName)
				{
					questObjectiveCaller.ObjectiveReachedEventHandler += CompleteObjective;
					worldPositon = questObjectiveCaller.transform.position;
				}
			}
		}
	}
	//TODO: After implementing notification queue, add completing objectives to queue
	/// <summary>
	/// Gets called when any of the subscribed event callers gets called
	/// </summary>
	/// <param name="caller"></param>
	/// <param name="sender"></param>
	public void CompleteObjective(QuestObjectiveCaller caller, string sender)
	{
		//Checks to see if the sender string is the one specified
		if (sender == targetSender)
		{
			//If it's not visible we don't want the player to be making progress
			if (isVisible)
			{
				amountCompleted++;
				if (amountCompleted >= amountToComplete)
				{
					isCompleted = true;
				}
				//Unsubscribe from the event
				//TODO:make this optional(unless it's on a destroy)
				caller.ObjectiveReachedEventHandler -= CompleteObjective;
			}
		}
	}
}
