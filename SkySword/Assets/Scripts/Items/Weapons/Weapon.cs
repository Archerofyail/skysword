﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public enum WeaponType
{
	OneHSword = 0,
	TwoHSword = 1,
	DoubleBladedSword = 2,
	ChainScythe = 3,
}


[Serializable]
public class Weapon : Item
{
	public WeaponType WeaponType;

	public Weapon(string [] keys, float[] values, WeaponType weaponType) : base(keys, values)
	{
		WeaponType = weaponType;
		enumType = weaponType.ToString();
		//itemPrefab = (GameObject)Resources.Load("Prefabs/" + WeaponType);
	}

	public Weapon(string[] keys, float[] values, WeaponType weaponType, string name)
		: base(keys, values)
	{
		WeaponType = weaponType;
		enumType = weaponType.ToString();
		ItemName = name;
		//itemPrefab = (GameObject)Resources.Load("Prefabs/" + WeaponType);
	}

	public Weapon(int typeInt)
	{
		WeaponType = (WeaponType) typeInt;
		enumType = WeaponType.ToString();
		//itemPrefab = (GameObject)Resources.Load("Prefabs/" + WeaponType);
	}

	public Weapon()
	{
		WeaponType = (WeaponType) typeInt;
		enumType = WeaponType.ToString();
		//itemPrefab = (GameObject) Resources.Load("Prefabs/" + WeaponType);
	}


	public void SetStats(string[] keys, int[] values, WeaponType weaponType)
	{
		WeaponType = weaponType;
		enumType = weaponType.ToString();
		SetStats(keys, values);
	}

	public override void GetRandom()
	{
		var weaponType = (WeaponType)Random.Range(0, 4);
		enumType = WeaponType.ToString();
		typeInt = (int)weaponType;
		SetStats(new[]
		{
			"Damage", 
			"Speed"
		},
		new[]
		{
			Random.Range(6, 13), 
			Random.Range(0, 2)
		},
		weaponType);
		switch (Random.Range(0, 3))
		{
			case 0:
			{
				ItemName += "Amazing";
			}
			break;
			case 1:
			{
				ItemName += "Powerful";
			}
			break;
			case 2:
			{
				ItemName += "Legendary";
			}
			break;
			case 3:
			{
				ItemName += "Weak";
			}
			break;

		}
		switch (weaponType)
		{
			case WeaponType.OneHSword:
				ItemName += " 1-Handed Sword";
				break;
			case WeaponType.TwoHSword:
				ItemName += " 2-Handed Sword";

				break;
			case WeaponType.DoubleBladedSword:
				ItemName += " Double-Bladed Sword";

				break;
			case WeaponType.ChainScythe:
				ItemName += " Scythe";

				break;
			default:
				throw new ArgumentOutOfRangeException();
		}
		switch (Random.Range(0, 3))
		{
			case 0:
			{
				ItemName += " of Ultimate Destiny";
			}
			break;
			case 1:
			{
				ItemName += " of Morga'Thul";
			}
			break;
			case 2:
			{
				ItemName += " of Trendiness";
			}
			break;
			case 3:
			{
				ItemName += " of Legend";
			}
			break;
		}
	}

	public override Item Clone()
	{
		return new Weapon(Stats.Keys.ToArray(), Stats.Values.ToArray(), WeaponType, ItemName);
	}
}
