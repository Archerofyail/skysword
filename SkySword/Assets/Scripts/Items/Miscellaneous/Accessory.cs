﻿using System;
using System.Collections.Generic;
using System.Collections;
using Random = UnityEngine.Random;

public enum AccessoryType
{
	Ring,
	Necklace,

}

public class Accessory : Item
{
	public AccessoryType accessoryType;
	public Accessory(int typeInt)
	{
		Stats = new Dictionary<string, float>();
		accessoryType = (AccessoryType) typeInt;
	}

	public Accessory()
	{
		
	}

	public override void GetRandom()
	{
		accessoryType = (AccessoryType) Random.Range(0, Enum.GetValues(typeof (AccessoryType)).Length);
		#region Name Gen
		int firstRand = Random.Range(1, 4);
		switch (firstRand)
		{
			case 1:
			{
				ItemName += "Amazing ";
			}
			break;
			case 2:
			{
				ItemName += "Stupendous ";
			}
			break;
			case 3:
			{
				ItemName += "Glorious ";
			}
			break;
		}
		ItemName += accessoryType + " ";
		int secondRand = Random.Range(1, 4);
		switch (secondRand)
		{
			case 1:
			{
				ItemName += "of Glu'Gol";
			}
			break;
			case 2:
			{
				ItemName += "of Generosity";
			}
			break;
			case 3:
			{
				//ItemName += "";
			}
			break;
		}
		#endregion
		#region Stats Gen
		int randInt = Random.Range(0, 3);
		switch (randInt)
		{
			case 1:
			{
				Stats.Add("Defense", 4);
			}
			break;
			case 2:
			{
				Stats.Add("Damage", 3);
			}
			break;
			case 3:
			{
				Stats.Add("Reflexes", 3);
			}
			break;
			default:
			{

			}
			break;
		}
		switch (randInt)
		{
			case 1:
			{
				Stats.Add("Damage", 3);

			}
			break;
			case 2:
			{
				Stats.Add("Reflexes", 3);
			}
			break;
			case 3:
			{
				Stats.Add("Defense", 3);
			}
			break;
			default:
			{

			}
			break;
		}
		#endregion
	}

	public override Item Clone()
	{
		return new Accessory(0);
	}
}
