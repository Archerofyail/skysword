﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using Random = UnityEngine.Random;


/// <summary>
/// Generates a random Item that can be picked up by a player
/// </summary>
public class Pickup : MonoBehaviour
{
	public Item Item;
	[SerializeField]
	private bool isPreset;

	private void Start()
	{
		if (!isPreset)
		{
			List<Type> itemTypes = new List<Type>();

			foreach (var type in Assembly.GetExecutingAssembly().GetTypes())
			{
				if (type.IsSubclassOf(typeof (Item)))
				{
					itemTypes.Add(type);
				}
			}
			int randNum = Random.Range(0, itemTypes.Count);
			Item = (Item) Activator.CreateInstance(itemTypes[randNum]);
			Item.GetRandom();
		}
	}
}
