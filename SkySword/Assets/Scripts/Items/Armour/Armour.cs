﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public enum ArmourType
{
	Helmet = 0,
	ShoulderPads = 1,
	Chest = 2,
	Leggings = 3,
	Boots = 4,
}

[Serializable]
public class Armour : Item
{
	public ArmourType ArmourType;

	public Armour(string[] keys, float[] values, ArmourType armourType, string name)
		: base(keys, values)
	{
		ArmourType = armourType;
		enumType = armourType.ToString();
		ItemName = name;
		itemPrefab = (GameObject) Resources.Load("Prefabs/" + armourType);
	}

	public Armour(int typeInt)
	{
		ArmourType = (ArmourType)typeInt;
		enumType = ArmourType.ToString();
	}

	public Armour()
	{

	}

	public void SetStats(string[] keys, int[] values, ArmourType armourType)
	{
		ArmourType = armourType;
		enumType = armourType.ToString();
		SetStats(keys, values);
	}

	public override void GetRandom()
	{
		var armourType = (ArmourType)Random.Range(0, 5);
		enumType = armourType.ToString();
		typeInt = (int)armourType;
		itemPrefab = (GameObject)Resources.Load("Prefabs/" + armourType);
		SetStats(new[]
		{
			"Defense", 
			"Weight"
		},
		new[]
		{
			Random.Range(6, 13), 
			Random.Range(2, 6)
		},
		armourType);
		switch (Random.Range(0, 3))
		{
			case 0:
			{
				ItemName += "Amazing";
			}
			break;
			case 1:
			{
				ItemName += "Powerful";
			}
			break;
			case 2:
			{
				ItemName += "Legendary";
			}
			break;
			case 3:
			{
				ItemName += "Weak";
			}
			break;
			default:
			{

			}
			break;

		}
		switch (armourType)
		{
			case ArmourType.Helmet:
			ItemName += " Helmet";
			break;
			case ArmourType.ShoulderPads:
			ItemName += " ShoulderPads";
			break;
			case ArmourType.Chest:
			ItemName += " Chest";
			break;
			case ArmourType.Leggings:
			ItemName += " Leggings";
			break;
			case ArmourType.Boots:
			ItemName += " Boots";
			break;
		}
		switch (Random.Range(0, 3))
		{
			case 0:
			{
				ItemName += " of Ultimate Destiny";
			}
			break;
			case 1:
			{
				ItemName += " of Morga'Thul";
			}
			break;
			case 2:
			{
				ItemName += " of Trendiness";
			}
			break;
			case 3:
			{
				ItemName += " of Legend";
			}
			break;
		}
	}

	public override Item Clone()
	{
		return new Armour(Stats.Keys.ToArray(), Stats.Values.ToArray(), ArmourType, ItemName);
	}
}
