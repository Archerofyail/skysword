﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Item
{
	public string ItemName;
	public Dictionary<string, float> Stats;
	public GameObject itemPrefab;
	public Texture2D itemImage;
	public int typeInt;
	public string enumType;
	
	public Item(string[] keys, float[] values)
	{
		Stats = new Dictionary<string, float>();
		int i = 0;
		foreach (var key in keys)
		{
			Stats.Add(key, values[i]);
			i++;
		}
	}

	public Item(Item item)
	{
		Stats = new Dictionary<string, float>(item.Stats);
		ItemName = item.ItemName;
		if (item.itemImage)
		{
			itemImage = item.itemImage;
		}
		if (item.itemPrefab)
		{
			itemPrefab = item.itemPrefab;
		}
	}

	public Item() 
	{
		Stats = new Dictionary<string, float>();
	}


	public void SetStats(string[] keys, int[] values)
	{
		if (Stats == null)
		{
			Stats = new Dictionary<string, float>();
		}
		int i = 0;
		foreach (var key in keys)
		{
			Stats.Add(key, values[i]);
			i++;
		}
	}

	public string FormattedStats()
	{
		string statsStringFormatted = "";
		int count = 0;
		foreach (var stat in Stats.Keys)
		{
			statsStringFormatted += stat + ": ";
			float value;
			if (Stats.TryGetValue(stat, out value))
			{
				statsStringFormatted += value + (count >= Stats.Keys.Count-1 ? "" : "\n");
			}
			else
			{
				throw new KeyNotFoundException("The key " + stat + " was not found in the dictionary");
			}
			count++;
		}
		return statsStringFormatted;
	}

	public virtual void GetRandom()
	{
		ItemName = "Not a specific item";
	}

	public virtual Item Clone()
	{
		return new Item();
	}
}
