﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Collections;

public class QuestGiverNameEditor : EditorWindow
{
	private List<string> questGiverNames;
	private int indexToRemove = -1;

	private Vector2 scrollPos;

	[MenuItem("Window/Quest Giver Editor")]
	public static void ShowWindow()
	{
		GetWindow<QuestGiverNameEditor>("Quest Giver Name Editor");
	}

	void OnGUI()
	{
		if (questGiverNames == null)
		{
			questGiverNames = QuestGiverNameManager.GetQuestGiverNames();
		}
		scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
		EditorGUILayout.BeginVertical();
		for(int i = 0; i < questGiverNames.Count; i++)
		{

			EditorGUILayout.BeginHorizontal();
			questGiverNames[i] = EditorGUILayout.TextField(questGiverNames[i]);
			if (GUILayout.Button("Delete"))
			{
				indexToRemove = i;
			}
			EditorGUILayout.EndHorizontal();
		}
		if (indexToRemove >= 0)
		{
			questGiverNames.RemoveAt(indexToRemove);
			indexToRemove = -1;
		}
		EditorGUILayout.EndVertical();
		EditorGUILayout.EndScrollView();

		EditorGUILayout.BeginHorizontal();
		if (GUILayout.Button("Add Name"))
		{
			questGiverNames.Add("");
		}
		if (GUILayout.Button("Save Names"))
		{
			QuestGiverNameManager.SaveQuestGiverNames(questGiverNames);
		}
		if (GUILayout.Button("Load Names"))
		{
			questGiverNames = QuestGiverNameManager.GetQuestGiverNames(true);
		}
		EditorGUILayout.EndHorizontal();
	}
}
