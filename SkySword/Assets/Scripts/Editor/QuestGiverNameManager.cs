﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;
using System.Collections;

public static class QuestGiverNameManager
{

	private static List<string> questGiverNames;

	public delegate void QuestGiverNamesChangedEvent();

	public static event QuestGiverNamesChangedEvent QuestGiverNamesChangedEventHandler;

	public static List<string> GetQuestGiverNames(bool forceReload = false)
	{
		if (questGiverNames != null && !forceReload)
		{
			if (questGiverNames.Count > 0)
			{
				return questGiverNames;
			}
		}

		questGiverNames = new List<string>();
		var nameStream = new FileStream(Directory.GetCurrentDirectory() + "\\DefaultData\\QuestGiverNames.xml",
			FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read);
		var nameReaderSettings = new XmlReaderSettings
		{
			CloseInput = true
		};
		using (var nameReader = XmlReader.Create(nameStream, nameReaderSettings))
		{
			if (nameReader.ReadToDescendant("GiverName"))
			{
				do
				{
					questGiverNames.Add(nameReader.ReadElementContentAsString());
				} while (nameReader.ReadToNextSibling("GiverName"));
			}
			nameReader.Close();
		}
		return questGiverNames;
	}

	public static void SaveQuestGiverNames(List<string> namesToSave)
	{
		questGiverNames = new List<string>(namesToSave);
		var saveNameStream = new FileStream(Directory.GetCurrentDirectory() + "\\DefaultData\\QuestGiverNames.xml",
			FileMode.Create, FileAccess.Write, FileShare.Write);
		var saveNameSettings = new XmlWriterSettings
		{
			CloseOutput = true,
			Indent = true,
			IndentChars = "\t",
			NewLineOnAttributes = true,
			OmitXmlDeclaration = true
		};
		using (var saveNameWriter = XmlWriter.Create(saveNameStream, saveNameSettings))
		{
			saveNameWriter.WriteStartElement("QuestGiverNames");
			foreach (var questGiverName in namesToSave)
			{
				saveNameWriter.WriteElementString("GiverName", questGiverName);
			}
			saveNameWriter.WriteEndElement();
			saveNameWriter.Close();
		}
		if (QuestGiverNamesChangedEventHandler != null)
		{
			QuestGiverNamesChangedEventHandler();
		}
	}
}
