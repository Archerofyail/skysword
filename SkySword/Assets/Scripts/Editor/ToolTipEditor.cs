﻿using System;
using UnityEditor;

[CustomEditor(typeof(InfoToolTip))]
public class ToolTipEditor : Editor
{
	private string toolTipText;
	void OnEnable()
	{
		toolTipText = serializedObject.FindProperty("toolTip").stringValue;
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		DrawDefaultInspector();
		toolTipText = EditorGUILayout.TextArea(toolTipText);
		if (serializedObject.targetObject)
		{
			var toolTip = serializedObject.targetObject as InfoToolTip;
			if (toolTip)
			{
				toolTip.toolTip = toolTipText;
			}
		}
	}

}
