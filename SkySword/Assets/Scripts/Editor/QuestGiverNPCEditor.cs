﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(QuestGiver))]
public class QuestGiverNPCEditor : Editor
{
	public List<string> questGiverNames; 
	private int selectedQuestGiverName;
	private bool hasSubbedToEvent;

	void OnEnable()
	{
		if (!hasSubbedToEvent)
		{
			QuestGiverNameManager.QuestGiverNamesChangedEventHandler += ReloadQuestGiverNames;
			hasSubbedToEvent = true;
		}
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		if (questGiverNames == null)
		{
			questGiverNames = QuestGiverNameManager.GetQuestGiverNames();
			selectedQuestGiverName = questGiverNames.IndexOf(serializedObject.FindProperty("Name").stringValue);
		}
		if (!hasSubbedToEvent)
		{
			QuestGiverNameManager.QuestGiverNamesChangedEventHandler += ReloadQuestGiverNames;
		}
		selectedQuestGiverName = EditorGUILayout.Popup(selectedQuestGiverName, questGiverNames.ToArray());
		if (serializedObject.targetObject)
		{
			var questGiver = serializedObject.targetObject as QuestGiver;
			if (questGiver)
			{
				questGiver.Name = questGiverNames[selectedQuestGiverName];
			}
		}
	}

	private void ReloadQuestGiverNames()
	{
		questGiverNames = QuestGiverNameManager.GetQuestGiverNames(true);
	}
}
