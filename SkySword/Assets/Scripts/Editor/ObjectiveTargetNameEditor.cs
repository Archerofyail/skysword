﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ObjectiveTargetNameEditor : EditorWindow
{

	private List<string> objectiveTargetNames;
	private int indexToRemove = -1; 
	private Vector2 scrollPos = Vector2.zero;
	[MenuItem("Window/Object Target Names Editor")]
	static void GetWindow()
	{
		GetWindow<ObjectiveTargetNameEditor>(false, "Objective Target Name Editor");
	}

	void OnEnable()
	{
		objectiveTargetNames = ObjectiveEventNamesManager.GetEventNames();
	}

	void OnGUI()
	{
		scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
		EditorGUILayout.BeginVertical();
		for (int i = 0; i < objectiveTargetNames.Count; i++)
		{
			EditorGUILayout.BeginHorizontal();
			objectiveTargetNames[i] = EditorGUILayout.TextField(objectiveTargetNames[i]);
			if (GUILayout.Button("Delete"))
			{
				indexToRemove = i;
			}
			if (indexToRemove > -1)
			{
				objectiveTargetNames.RemoveAt(indexToRemove);
				indexToRemove = -1;
			}
			EditorGUILayout.EndHorizontal();
		}
		EditorGUILayout.EndVertical();
		EditorGUILayout.EndScrollView();
		EditorGUILayout.BeginHorizontal();
		if (GUILayout.Button("Add Event Name"))
		{
			objectiveTargetNames.Add("EventName");
		}
		if (GUILayout.Button("Save Event Names"))
		{
			ObjectiveEventNamesManager.SaveEventNames(objectiveTargetNames);
		}
		if (GUILayout.Button("Load Event Names"))
		{
			objectiveTargetNames = ObjectiveEventNamesManager.GetEventNames(true);
		}
		EditorGUILayout.EndHorizontal();
	}
}
