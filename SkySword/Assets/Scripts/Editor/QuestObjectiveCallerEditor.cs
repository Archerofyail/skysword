﻿using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(QuestObjectiveCaller))]
public class QuestObjectiveCallerEditor : Editor
{
	private List<string> objectiveTargetNames;
	private int selectedNameIndex;

	void OnEnable()
	{
		objectiveTargetNames = ObjectiveEventNamesManager.GetEventNames();
		selectedNameIndex = objectiveTargetNames.IndexOf(serializedObject.FindProperty("objectiveName").stringValue);
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		selectedNameIndex = EditorGUILayout.Popup(selectedNameIndex, objectiveTargetNames.ToArray());
		if (serializedObject.targetObject)
		{
			var questObjectiveCaller = serializedObject.targetObject as QuestObjectiveCaller;
			if (questObjectiveCaller)
			{
				questObjectiveCaller.objectiveName = objectiveTargetNames[selectedNameIndex];
			}
		}
	}
}
