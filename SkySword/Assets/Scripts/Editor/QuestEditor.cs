﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using UnityEditor;
using UnityEngine;

public class QuestEditor : EditorWindow
{
	private List<Quest> quests;
	private Vector2 scrollPos;
	private List<List<bool>> foldOutBools;

	private List<string> questGiverNames;
	private List<int> selectedQuestGiverIndex;

	public List<string> objectiveTargetNames;
	private List<List<int>> objectiveTargetSelectionList;

	private List<string> questObjectiveEventSenderList;
	private List<List<int>> questObjectEventSenderSelection;

	[MenuItem("Window/Quest Editor")]
	public static void ShowWindow()
	{
		GetWindow<QuestEditor>(false, "Quest Editor");
	}

	void OnEnable()
	{
		QuestGiverNameManager.QuestGiverNamesChangedEventHandler -= ReloadQuestGiverNames;
		QuestGiverNameManager.QuestGiverNamesChangedEventHandler += ReloadQuestGiverNames;
		ObjectiveEventNamesManager.ObjectEventNamesChangedEventHandler -= ReloadObjectiveEventNames;
		ObjectiveEventNamesManager.ObjectEventNamesChangedEventHandler += ReloadObjectiveEventNames;
		LoadQuests();
		LoadIndexesAndBools();
	}

	private void OnFocus()
	{
		LoadQuests();
	}

	private void LoadIndexesAndBools()
	{
		if (questObjectiveEventSenderList == null)
		{
			questObjectiveEventSenderList = new List<string>
			{
				ObjectiveEventNames.ObjectDestroyed,
				ObjectiveEventNames.PlayerTrigger
			};
			foreach (var keyCode in Enum.GetNames(typeof(KeyCode)))
			{
				if (keyCode.Length <= 4)
				{
					questObjectiveEventSenderList.Add(keyCode.ToLower());
				}
			}
		}
		foldOutBools = new List<List<bool>>(2);
		questObjectEventSenderSelection = new List<List<int>>();
		objectiveTargetSelectionList = new List<List<int>>();
		selectedQuestGiverIndex = new List<int>();
		if (questGiverNames == null)
		{
			questGiverNames = QuestGiverNameManager.GetQuestGiverNames(true);
		}
		if (objectiveTargetNames == null)
		{
			objectiveTargetNames = ObjectiveEventNamesManager.GetEventNames(true);
		}
		int i = 0;
		foreach (var quest in quests)
		{
			foldOutBools.Add(new List<bool>());
			questObjectEventSenderSelection.Add(new List<int>());
			selectedQuestGiverIndex.Add(questGiverNames.IndexOf(quest.questGiver));
			objectiveTargetSelectionList.Add(new List<int>());
			foldOutBools[i].Add(true);
			foldOutBools[i].Add(true);
			foreach (var objective in quest.questObjectives)
			{
				objectiveTargetSelectionList[i].Add(objectiveTargetNames.IndexOf(objective.targetName));
				questObjectEventSenderSelection[i].Add(questObjectiveEventSenderList.IndexOf(objective.targetSender));
				foldOutBools[i].Add(false);
			}
			i++;
		}
	}

	private void OnGUI()
	{

		EditorGUILayout.BeginVertical();
		scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
		if (quests != null)
		{
			var questsToRemove = new List<int>();
			for (int i = 0; i < quests.Count; i++)
			{
				var quest = quests[i];
				if (foldOutBools == null)
				{
					LoadIndexesAndBools();
				}
				if (objectiveTargetNames == null)
				{
					objectiveTargetNames = ObjectiveEventNamesManager.GetEventNames();
				}
				if (questGiverNames == null)
				{
					questGiverNames = QuestGiverNameManager.GetQuestGiverNames();
				}
				if (questGiverNames.Count == 0)
				{
					questGiverNames = QuestGiverNameManager.GetQuestGiverNames(true);
				}
				foldOutBools[i][0] = EditorGUILayout.Foldout(foldOutBools[i][0], quest.questName);
				EditorGUI.indentLevel++;
				if (foldOutBools[i][0])
				{

					EditorGUILayout.BeginVertical();

					EditorGUILayout.BeginHorizontal();
					quest.questName = EditorGUILayout.TextField("Name", quest.questName, GUILayout.MinWidth(150));
					selectedQuestGiverIndex[i] = EditorGUILayout.Popup(selectedQuestGiverIndex[i], questGiverNames.ToArray());
					quest.questGiver = questGiverNames[selectedQuestGiverIndex[i] >= questGiverNames.Count ? 0 : selectedQuestGiverIndex[i]];
					EditorGUILayout.EndHorizontal();
					EditorGUILayout.LabelField("Description");
					quest.questDescription = EditorGUILayout.TextArea(quest.questDescription, GUILayout.MinWidth(5));

					EditorGUILayout.BeginHorizontal();
					quest.isAvailable = EditorGUILayout.Toggle("Is Available", quest.isAvailable, GUILayout.MaxWidth(150));
					quest.questStatus = (QuestStatus)EditorGUILayout.EnumPopup(GUIContent.none, quest.questStatus, GUILayout.MaxWidth(115));
					quest.skillPointsGained = EditorGUILayout.IntField("Skill Points Gained", quest.skillPointsGained,
						GUILayout.MinWidth(5));
					EditorGUILayout.EndHorizontal();
					foldOutBools[i][1] = EditorGUILayout.Foldout(foldOutBools[i][1], "Objectives");
					EditorGUI.indentLevel++;
					if (foldOutBools[i][1])
					{
						int j = 2;
						var objectivesToRemove = new List<int>();
						foreach (var objective in quest.questObjectives)
						{
							foldOutBools[i][j] = EditorGUILayout.Foldout(foldOutBools[i][j], "Objective");

							if (foldOutBools[i][j])
							{
								EditorGUI.indentLevel++;
								EditorGUILayout.BeginHorizontal();
								objectiveTargetSelectionList[i][j - 2] = EditorGUILayout.Popup("Target Name",
									objectiveTargetSelectionList[i][j - 2], objectiveTargetNames.ToArray(), GUILayout.MinWidth(5));
								objective.targetName = objectiveTargetNames[objectiveTargetSelectionList[i][j - 2]];
								questObjectEventSenderSelection[i][j - 2] = EditorGUILayout.Popup("Target Sender",
									questObjectEventSenderSelection[i][j - 2], questObjectiveEventSenderList.ToArray(),
									GUILayout.MinWidth(5));
								objective.targetSender = questObjectiveEventSenderList[questObjectEventSenderSelection[i][j - 2]];
								EditorGUILayout.EndHorizontal();
								objective.objectiveDescription = EditorGUILayout.TextField("Description", objective.objectiveDescription);
								objective.worldPositon = EditorGUILayout.Vector3Field("World Position", objective.worldPositon);
								EditorGUILayout.BeginHorizontal();
								objective.isVisible = EditorGUILayout.Toggle("Is Visible", objective.isVisible, GUILayout.MaxWidth(175));
								objective.amountToComplete = EditorGUILayout.IntField("Amount to Complete", objective.amountToComplete);
								if (GUILayout.Button("Delete Objective"))
								{
									foldOutBools[i].RemoveAt(j);
									objectivesToRemove.Add(j - 2);
								}
								EditorGUILayout.EndHorizontal();
								EditorGUI.indentLevel--;
							}
							j++;
						}
						EditorGUI.indentLevel--;
						foreach (var objective in objectivesToRemove)
						{
							quest.questObjectives.RemoveAt(objective);
						}
						EditorGUILayout.BeginHorizontal();
						GUILayout.Space(50);
						if (GUILayout.Button("Add Objective", GUILayout.ExpandWidth(true)))
						{
							quest.questObjectives.Add(new QuestObjective());
							foldOutBools[i].Add(true);
						}
						EditorGUILayout.EndHorizontal();
						EditorGUILayout.Space();
						EditorGUI.indentLevel = 1;

					}

					EditorGUILayout.EndVertical();
					if (GUILayout.Button("Delete Quest"))
					{
						foldOutBools.RemoveAt(i);
						questsToRemove.Add(i);
						LoadIndexesAndBools();
					}
				}
				EditorGUI.indentLevel = 0;
			}
			EditorGUI.indentLevel = 0;
			foreach (var quest in questsToRemove)
			{
				quests.RemoveAt(quest);
			}
		}
		EditorGUILayout.EndScrollView();
		EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));
		if (GUILayout.Button("Save Quests"))
		{
			SaveQuests();
		}
		if (GUILayout.Button("Add Quest"))
		{
			quests.Add(new Quest());
			foldOutBools.Add(new List<bool>(2) { true, true, false });
		}
		if (GUILayout.Button("Load Quests"))
		{
			LoadQuests();
			LoadIndexesAndBools();
		}
		if (GUILayout.Button("Load Backup Quests"))
		{
			LoadQuests();
			LoadIndexesAndBools();
		}
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.EndVertical();
	}


	private void ReloadQuestGiverNames()
	{
		questGiverNames = QuestGiverNameManager.GetQuestGiverNames(true);
	}

	private void ReloadObjectiveEventNames()
	{
		objectiveTargetNames = ObjectiveEventNamesManager.GetEventNames(true);
	}

	private void SaveQuests()
	{
		if (quests.Count >= 2)
		{
			var saveStream = new FileStream(Directory.GetCurrentDirectory() + "\\DefaultData\\DefaultQuestConfig.xml",
				FileMode.Create, FileAccess.Write, FileShare.Write);
			var saveSettings = new XmlWriterSettings
			{
				CloseOutput = true,
				Indent = true,
				NewLineOnAttributes = true,
				IndentChars = "\t"
			};
			using (var saveWriter = XmlWriter.Create(saveStream, saveSettings))
			{
				saveWriter.WriteStartElement(XmlNames.Quests);
				foreach (var quest in quests)
				{
					SaveQuest(saveWriter, quest);
				}
				saveWriter.WriteEndElement();
				saveWriter.Close();
			}
		}
	}

	private static void SaveQuest(XmlWriter saveWriter, Quest questToSave)
	{
		saveWriter.WriteStartElement(XmlNames.Quest);
		saveWriter.WriteAttributeString(XmlNames.QuestName, questToSave.questName);
		saveWriter.WriteAttributeString(XmlNames.QuestGiver, questToSave.questGiver);
		saveWriter.WriteAttributeString(XmlNames.QuestDescription, questToSave.questDescription);
		saveWriter.WriteAttributeString(XmlNames.QuestIsAvailable, questToSave.isAvailable.ToString());
		saveWriter.WriteAttributeString(XmlNames.SkillPointsGained, questToSave.skillPointsGained.ToString(CultureInfo.InvariantCulture));
		saveWriter.WriteAttributeString(XmlNames.QuestStatus, questToSave.questStatus.ToString());
		saveWriter.WriteStartElement(XmlNames.QuestObjectives);
		foreach (var objective in questToSave.questObjectives)
		{
			saveWriter.WriteStartElement(XmlNames.QuestObjective);
			saveWriter.WriteAttributeString(XmlNames.QuestObjectiveIsComplete, objective.isCompleted.ToString());
			saveWriter.WriteAttributeString(XmlNames.QuestObjectiveTargetName, objective.targetName);
			saveWriter.WriteAttributeString(XmlNames.QuestObjectiveTargetSender, objective.targetSender);
			saveWriter.WriteAttributeString(XmlNames.QuestObjectiveDescription, objective.objectiveDescription);
			saveWriter.WriteAttributeString(XmlNames.QuestObjectiveAmountToComplete, objective.amountToComplete.ToString(CultureInfo.InvariantCulture));
			saveWriter.WriteAttributeString(XmlNames.QuestObjectiveAmountCompleted, objective.amountCompleted.ToString(CultureInfo.InvariantCulture));
			saveWriter.WriteAttributeString(XmlNames.QuestObjectiveIsVisible, objective.isVisible.ToString());
			saveWriter.WriteStartElement(XmlNames.QuestObjectiveWorldPosition);
			saveWriter.WriteAttributeString("x", objective.worldPositon.x.ToString(CultureInfo.InvariantCulture));
			saveWriter.WriteAttributeString("y", objective.worldPositon.y.ToString(CultureInfo.InvariantCulture));
			saveWriter.WriteAttributeString("z", objective.worldPositon.z.ToString(CultureInfo.InvariantCulture));
			saveWriter.WriteEndElement();
			saveWriter.WriteEndElement();
		}
		saveWriter.WriteEndElement();
		saveWriter.WriteEndElement();
	}

	private void LoadQuests()
	{
		var questFileInfo = new FileInfo(Directory.GetCurrentDirectory() + "\\DefaultData\\DefaultQuestConfig.xml");
		if (questFileInfo.Exists)
		{
			var loadStream = new FileStream(Directory.GetCurrentDirectory() + "\\DefaultData\\DefaultQuestConfig.xml",
				FileMode.Open, FileAccess.Read, FileShare.Read);
			var questReadSettings = new XmlReaderSettings { CloseInput = true };
			using (var questReader = XmlReader.Create(loadStream, questReadSettings))
			{
				quests = new List<Quest>();
				quests = GameDataManager.LoadQuests(questReader);
			}
		}
	}
}
