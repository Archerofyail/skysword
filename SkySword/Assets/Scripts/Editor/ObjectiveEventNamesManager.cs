﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;

[ExecuteInEditMode]
public static class ObjectiveEventNamesManager
{
	private static List<string> ObjectiveTargetNames;

	public delegate void ObjectEventNamesChangedEvent();

	public static event ObjectEventNamesChangedEvent ObjectEventNamesChangedEventHandler;

	public static List<string> GetEventNames(bool forceReload = false)
	{
		if (ObjectiveTargetNames != null && !forceReload)
		{
			if (ObjectiveTargetNames.Count > 0)
			{
				return ObjectiveTargetNames;
			}
		}
		ObjectiveTargetNames = new List<string>();
		var objectiveTargetNamesStream =
			new FileStream(Directory.GetCurrentDirectory() + "\\DefaultData\\ObjectiveTargetNames.xml", FileMode.Open,
				FileAccess.Read, FileShare.Read);
		var nameReaderSettings = new XmlReaderSettings
		{
			CloseInput = true,
		};
		using (var namesReader = XmlReader.Create(objectiveTargetNamesStream, nameReaderSettings))
		{
			if (namesReader.ReadToFollowing("Names"))
			{
				if (namesReader.ReadToDescendant("EventName"))
				{
					do
					{
						ObjectiveTargetNames.Add(namesReader.ReadElementContentAsString());
					} while (namesReader.ReadToNextSibling("EventName"));
				}
			}
			namesReader.Close();
		}

		return ObjectiveTargetNames;
	}

	public static void SaveEventNames(List<string> eventNames)
	{
		ObjectiveTargetNames = eventNames;
		var objectiveTargetNamesStream =
			new FileStream(Directory.GetCurrentDirectory() + "\\DefaultData\\ObjectiveTargetNames.xml", FileMode.Create,
				FileAccess.Write, FileShare.Write);
		var nameReaderSettings = new XmlWriterSettings
		{
			CloseOutput = true,
			Indent = true,
			IndentChars = "\t",
			NewLineOnAttributes = true
		};
		using (var namesWriter = XmlWriter.Create(objectiveTargetNamesStream, nameReaderSettings))
		{
			namesWriter.WriteStartElement("Names");
			foreach (var targetName in eventNames)
			{
				namesWriter.WriteStartElement("EventName");
				namesWriter.WriteValue(targetName);
				namesWriter.WriteEndElement();
			}
			namesWriter.WriteEndElement();
			namesWriter.Flush();
			namesWriter.Close();
		}
		if (ObjectEventNamesChangedEventHandler != null)
		{
			ObjectEventNamesChangedEventHandler();
		}
	}
}
