﻿using UnityEngine;
using System.Collections;

public class CinematicEventTrigger : MonoBehaviour
{
	public bool playerReachedEvent { get; private set; }
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
		{
			playerReachedEvent = true;
		}
	}
}
