﻿using System.Security.Cryptography;
using UnityEngine;
using System.Collections;

public class CinematicCameraEventHandler : MonoBehaviour
{
	public Camera firstCamera;
	public Camera lastCameraInSequence;
	public bool isBeingDestroyed;

	void Start()
	{
	}

	void Update()
	{
		if (!lastCameraInSequence)
		{
			PlayerControlHandler.EnablePlayerControl();
			Destroy(gameObject);
		}
		if (firstCamera)
		{
			if (firstCamera.enabled && !PlayerControlHandler.AreScriptsEnabled)
			{
				PlayerControlHandler.DisablePlayerControl();
			}
		}
	}
}
