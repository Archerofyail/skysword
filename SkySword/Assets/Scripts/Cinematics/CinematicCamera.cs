﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A customizable cinematic camera script
/// </summary>
public class CinematicCamera : MonoBehaviour
{
	[SerializeField]
	private bool isFirstCamera;
	[SerializeField]
	private float rotationSpeed = 50f;
	private bool isPlaying;
	[SerializeField]
	private bool isRotating;
	[SerializeField]
	private bool isFollowing;
	[SerializeField]
	private bool isMatchingHeight;//If it's true the height of the camera will change with the target object
	private bool cameraWasEnabled;
	public GameObject target;//The target to look at. leave null if you don't want something specific to focus on
	public Camera nextCamera;//The next camera to activate. Do not make this the camera that activated you, else the fiery recursion gods will smite thee
	public CinematicEventTrigger cinematicEventTrigger;
	[SerializeField]
	private Vector3 distanceFromTarget;//The distance the camera is from the target's transform.position.
	[SerializeField]
	private float timeUntilDestroy = 6f;//The time until Destroy is called on the GameObject. 
	[SerializeField]
	private float fadeInTime = 2f;
	[SerializeField]
	private float fadeOutTime = 4f;
	[SerializeField]
	private Vector3 movementOverTime;
	private float guiTransparency = 1f;
	private float timeSinceActive;
	[SerializeField]
	private Texture blackTexture;//Leave this blank if you don't want any fading

	void Start()
	{
	}

	void OnDestroy()
	{

		if (nextCamera)
		{
			
			nextCamera.enabled = true;
		}
	}

	void Update()
	{
		if (cinematicEventTrigger)
		{
			if (cinematicEventTrigger.playerReachedEvent)
			{
				GetComponent<Camera>().enabled = true;
			}
		}
		if (GetComponent<Camera>().enabled && !cameraWasEnabled)
		{
			cameraWasEnabled = true;
			Destroy(gameObject, timeUntilDestroy);
			isPlaying = true;
			if (target)
			{

				transform.position = new Vector3(target.transform.position.x + distanceFromTarget.x,
					target.transform.position.y + distanceFromTarget.y, target.transform.position.z + distanceFromTarget.z);
			}
		}
		if (isPlaying)
		{
			if (target)
			{
				if (isFollowing)
				{
					transform.position = new Vector3(transform.position.x + distanceFromTarget.x,
						isMatchingHeight
							? target.transform.position.y + distanceFromTarget.y
							: transform.position.y + distanceFromTarget.y, target.transform.position.z - distanceFromTarget.z);
				}
				if (isRotating)
				{
					transform.RotateAround(target.transform.position, Vector3.up, rotationSpeed * Time.deltaTime);
				}


				transform.LookAt(target.transform);
			}
			transform.Translate(new Vector3(movementOverTime.x*Time.deltaTime, movementOverTime.y*Time.deltaTime,
				movementOverTime.z*Time.deltaTime));
			timeSinceActive += Time.deltaTime;
			if (timeSinceActive <= fadeInTime)
			{
				guiTransparency = Mathf.Clamp01(1 - (timeSinceActive / fadeInTime));
			}
			else if (timeSinceActive >= fadeOutTime)
			{
				guiTransparency = Mathf.Clamp01(1 - ((timeUntilDestroy - timeSinceActive) / fadeOutTime));
			}
			if (Input.GetKeyDown(KeyCode.Space))
			{
				GUI.color = new Color(0, 0, 0, 1);
				Destroy(gameObject);
			}
		}
	}

	void Play()
	{
		isPlaying = true;
	}

	void Stop()
	{
		isPlaying = false;
	}

	private void OnGUI()
	{
		if (isPlaying)
		{
			GUI.color = new Color(0, 0, 0, guiTransparency);
			GUI.backgroundColor = new Color(0, 0, 0, guiTransparency);
			if (blackTexture)
			{
				GUI.DrawTexture(new Rect(0, 0, Screen.width + 15, Screen.height + 10), blackTexture, ScaleMode.StretchToFill);
			}
		}
	}
}
