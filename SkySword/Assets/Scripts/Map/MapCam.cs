﻿using UnityEngine;

public class MapCam : MonoBehaviour
{
	public GameObject target;
	public float distanceAbovePlayer;
	public bool rotateWithPlayer;

	public Texture2D mapBorderTex;

	public Camera miniMapCam;

	void Start()
	{
		miniMapCam = GetComponent<Camera>();
	}

	void Update()
	{
		if (!MenuManager.isAMenuEnabled)
		{
			miniMapCam.enabled = true;
		}
		else
		{
			miniMapCam.enabled = false;
		}
		if (target)
		{
			if (rotateWithPlayer)
			{
				var eulerRot = transform.rotation.eulerAngles;
				transform.rotation = Quaternion.Euler(eulerRot.x, eulerRot.y, target.transform.rotation.eulerAngles.z);
			}
			transform.position = Vector3.Lerp(transform.position, target.transform.position + (Vector3.up * distanceAbovePlayer), 0.1f);
		}
		else
		{
			target = GameObject.Find("Player");
		}
	}

	private void OnGUI()
	{
		if (!MenuManager.isAMenuEnabled)
		{
			GUI.DrawTexture(
				new Rect(GetComponent<Camera>().pixelRect.x, Screen.height - GetComponent<Camera>().pixelRect.y - GetComponent<Camera>().pixelHeight, GetComponent<Camera>().pixelRect.width + 10,
					GetComponent<Camera>().pixelRect.height + 5), mapBorderTex);
		}
	}
}
