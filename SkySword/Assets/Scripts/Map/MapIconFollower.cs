﻿using UnityEngine;
using System.Collections;

public class MapIconFollower : MonoBehaviour
{
	public GameObject target;
	void Start()
	{

	}

	void Update()
	{
		if (target)
		{
			transform.position = Vector3.Lerp(transform.position, target.transform.position, 1f);
		}
	}
}
