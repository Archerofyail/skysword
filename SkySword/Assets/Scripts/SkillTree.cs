﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;

public class SkillTree
{
	public List<List<Skill>> skills;
	public List<int> pointsInTiers;
	public string treeName;

	public SkillTree(List<List<Skill>> skills, string treeName)
	{
		this.skills = skills;
		pointsInTiers = new List<int>();
		foreach (var skillTier in skills)
		{
			pointsInTiers.Add(skillTier.Max(skill => skill.tier) + 1);
		}
		this.treeName = treeName;
		SumPointsInTiers();
	}

	public SkillTree(List<List<Skill>> skills, List<int> pointsInTiers, string treeName)
	{
		this.skills = skills;
		this.pointsInTiers = pointsInTiers;
		this.treeName = treeName;
		SumPointsInTiers();
	}

	public SkillTree()
	{
		skills = new List<List<Skill>>();
		pointsInTiers = new List<int>();
	}

	public void SumPointsInTiers()
	{
		pointsInTiers = new List<int>();
		for (int i = 0; i < skills.Count; i++)
		{
			pointsInTiers.Add(skills[i].Sum(skill => skill.rank));
		}
	}

	public IEnumerable<Skill> GetSkillsByName(IEnumerable<string> skillNames)
	{
		List<Skill> returnedSkills = new List<Skill>();
		foreach (var skillName in skillNames)
		{
			foreach (var skillList in skills)
			{
				foreach (var skill in skillList)
				{
					if (skill.skillName == skillName)
					{
						returnedSkills.Add(skill);
					}
				}
			}
		}
		return returnedSkills;
	}
}
