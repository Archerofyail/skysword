﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/// <summary>
/// Loads pickups from the loaded save game
/// </summary>
public class PickupPlacer : MonoBehaviour
{
	private GameObject player;
	public List<Item> itemList;

	void Start()
	{
		SceneManager.sceneLoaded += CheckToLoadPickups;
		itemList = new List<Item>();
	}

	void Update()
	{
		if (!player)
		{
			player = GameObject.Find("Player");
		}
		if (Input.GetKeyDown(KeyCode.L))
		{
			Instantiate(Resources.Load("Prefabs/Pickup"), player.transform.position + player.transform.forward * 4, Quaternion.identity);
		}
	}

	/// <summary>
	/// Loads in pickups after the currently loading level has been loaded. Only should be called from GameDataManager
	/// </summary>
	/// <param name="loadedGame">The save game that has been selected</param>
	public void CheckToLoadPickups(Scene scene, LoadSceneMode mode)
	{
		SaveGame loadedGame = GameDataManager.GetLoadedGame();
		if (loadedGame != null)
		{
			foreach (var pickup in loadedGame.pickupObjects)
			{
				var pickupObject =
					(GameObject)
						Instantiate(Resources.Load("Prefabs/PickupPreset"), pickup.position,
							Quaternion.Euler(pickup.eulerRotation));
				pickupObject.name = pickup.pickup.ItemName;
				pickupObject.GetComponent<Pickup>().Item = pickup.pickup;
				itemList.Add(pickup.pickup);
			}
		}
	}
}
