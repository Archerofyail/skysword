﻿using UnityEngine;

public class TravelCompanion : MonoBehaviour
{

	public GameObject target;

	void Start()
	{
		target = GameObject.Find("CompanionTarget");
	}

	private void Update()
	{
		if ((target.transform.position - transform.position).magnitude > 0.5f)
		{
			transform.position = Vector3.Lerp(transform.position, target.transform.position, 6f * Time.deltaTime);
		}
		if (target.transform.rotation != transform.rotation)
		{
			transform.rotation = Quaternion.Lerp(transform.rotation, target.transform.rotation, 6f * Time.deltaTime);
		}
	}
}
