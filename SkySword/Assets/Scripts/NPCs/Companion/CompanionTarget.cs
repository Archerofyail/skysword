﻿using System.Net;
using UnityEngine;

public class CompanionTarget : MonoBehaviour
{
	private Transform playerTransform;
	public Vector3 positionOffset = new Vector3(1.2f, 0.35f, 0);
	public bool isColliding;
	void Start()
	{
		Physics.IgnoreCollision(GetComponent<Collider>(), GameObject.Find("Companion").GetComponent<Collider>());
		playerTransform = GameObject.Find("Player").transform;
	}

	void Update()
	{
		transform.position = Vector3.Lerp(transform.position,
			playerTransform.position + playerTransform.TransformDirection(positionOffset), 1f);
		transform.rotation = Quaternion.Lerp(transform.rotation, playerTransform.rotation, 1f);
	}

	void OnCollisionStay(Collision other)
	{
		isColliding = true;
		transform.Translate((GetComponent<Rigidbody>().velocity - (other.rigidbody ? other.rigidbody.velocity : Vector3.zero)));
	}

	void OnCollisionExit(Collision other)
	{
		isColliding = false;
	}
}
