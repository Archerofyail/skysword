﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class QuestGiver : MonoBehaviour
{
	public List<Quest> quests;
	public string Name;
	
	void Start()
	{
		quests = QuestManager.GetQuestByQuestGiver(Name);
		foreach (var quest in quests)
		{
			quest.SetQuestObjectiveEvents();
		}
	}

	void Update()
	{

	}
}
