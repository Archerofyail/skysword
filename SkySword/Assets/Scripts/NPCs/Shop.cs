﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
[Serializable]
public class Shop
{
	public List<Item> InventoryItems;
	public Vector3 worldPos;
	public bool hasBeenVisited;

	public Shop(List<Item> inventory)
	{
		InventoryItems = new List<Item>(inventory);
	}

	public Shop()
	{
		InventoryItems = new List<Item>();
	}

	public void Buy(Item itemToBuy, bool isRemote)
	{
		if (isRemote)
		{
			Debug.Log("Made a remote purchase");
			CommonPlayerData.remotePurchaseTracker.StartCoroutine("PurchaseTimer", new RemotePurchase(itemToBuy, (worldPos.sqrMagnitude / 1000)));
		}
		else
		{
			CommonPlayerData.inventory.Add(itemToBuy);			
		}
	}

	public void Add(Item itemToAdd)
	{

		InventoryItems.Add(itemToAdd);
	}

	public void AddRange(Item[] itemsToAdd)
	{
		InventoryItems.AddRange(itemsToAdd);
	}
}
