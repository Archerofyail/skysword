﻿public static class Tags
{
	public const string Respawn = "Respawn";
	public const string Finish = "Finish";
	public const string EditorOnly = "EditorOnly";
	public const string MainCamera = "MainCamera";
	public const string Player = "Player";
	public const string GameController = "GameController";
	public const string Ground = "Ground";
	public const string GroundEnemy = "GroundEnemy";
	public const string PlayerSword = "PlayerSword";
	public const string AirEnemy = "AirEnemy";
	public const string Enemy = "Enemy";
	public const string EventController = "EventController";
	public const string Waypoint = "Waypoint";
	public const string FloatingText = "FloatingText";
	public const string Pickup = "Pickup";
	public const string ShopKeep = "ShopKeep";
	public const string QuestGiver = "QuestGiver";
}

public static class PreferenceKeys
{
	public const string x = "x";
	public const string y = "y";
	public const string z = "z";
	public const string IsInAir = "IsInAir";
	public const string SaveFileName = "SaveFileName";
}

public static class MenuClassNames
{
	public const string MainMenu = "MainMenu";
	public const string LoadGameMenu = "LoadGameMenu";
	public const string OptionsMenu = "OptionsMenu";
	public const string PauseMenu = "PauseMenu";
	public const string ShopMenu = "ShopMenu";
	public const string InventoryMenu = "InventoryMenu";
	public const string MapMenu = "MapMenu";
	public const string SkillTreeMenu = "SkillTreeMenu";
	public const string QuestLogMenu = "QuestLogMenu";
	public const string ViewQuestMenu = "ViewQuestMenu";
	public const string RemotePurchaseMenu = "RemotePurchaseMenu";
}

public static class ShopNames
{
	public const string TutorialShop = "Tutorial Shop";
}

public static class InputNames
{
	public const string Strafe = "Strafe";
	public const string ForwardBackward = "Forward/Backward";
	public const string LookHorizontal = "Look Horizontal";
	public const string LookVertical = "Look Vertical";
	public const string Jump = "Jump";
	public const string Interact = "Interact";
	public const string PrimaryAttack = "Primary Attack";
	public const string SecondaryAttack = "Secondary Attack";
	public const string OpenInventory = "Open Inventory";
	public const string OpenMap = "Open Map";
	public const string OpenQuestLog = "Open Quest Log";
	public const string OpenSkillTree = "Open Skill Tree";
	public const string ZoomMap = "Zoom Map";
	public const string DecreaseHeight = "Decrease Height";
	public const string DisableFlight = "Disable Flight";
	public const string Boost = "Boost";
	public const string OpenRemotePurchase = "Open Remote Purchase";
}

public static class XmlNames
{
	public const string SaveVersion = "SaveVersion";
	public const string SaveGame = "SaveGame";
	public const string SceneNameAttribute = "Scene";
	public const string Player = "Player";
	public const string IsInAirAttribute = "IsInAir";
	public const string Transform = "Transform";
	public const string Position = "Position";
	public const string Rotation = "Rotation";
	public const string Scale = "Scale";
	public const string Inventory = "inventory";
	public const string Equipped = "Equipped";
	public const string Item = "Item";
	public const string Name = "Name";
	public const string Type = "Type";
	public const string Stats = "Stats";
	public const string Stat = "Stat";
	public const string StatValueAttribute = "Value";
	public const string Shops = "Shops";
	public const string Shop = "Shop";
	public const string Pickups = "Pickups";
	public const string Pickup = "Pickup";
	public const string Quest = "Quest";
	public const string Quests = "Quests";
	public const string QuestName = "QuestName";
	public const string QuestGiver = "QuestGiver";
	public const string QuestDescription = "QuestDescription";
	public const string QuestObjectives = "QuestObjectives";
	public const string QuestObjective = "QuestObjective";
	public const string QuestObjectiveWorldPosition = "WorldPosition";
	public const string QuestIsAvailable = "IsAvailable";
	public const string SkillPointsGained = "SkillPointsGained";
	public const string QuestStatus = "QuestStatus";
	public const string QuestObjectiveIsComplete = "IsComplete";
	public const string QuestObjectiveTargetName = "TargetName";
	public const string QuestObjectiveTargetSender = "TargetSender";
	public const string QuestObjectiveDescription = "ObjectiveDescription";
	public const string QuestObjectiveAmountToComplete = "AmountToComplete";
	public const string QuestObjectiveAmountCompleted = "AmountCompleted";
	public const string QuestObjectiveIsVisible = "IsVisible";

}

/// <summary>
/// The names of objective reached events that are not input
/// </summary>
public static class ObjectiveEventNames
{
	public const string PlayerTrigger = "PlayerTrigger";
	public const string ObjectDestroyed = "ObjectDestroyed";
}