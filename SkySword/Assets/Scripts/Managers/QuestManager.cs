﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using UnityEngine;
using System.Collections;

/// <summary>
/// This class stores all the quests in the game. Each Quest Giver gets their quests from here
/// </summary>
public static class QuestManager
{
	public static List<Quest> allQuests;

	static QuestManager()
	{
		allQuests = new List<Quest>();
	}

	public static void Awake()
	{
		var questReaderSettings = new XmlReaderSettings {CloseInput = true};
		var questStream = new FileStream(Directory.GetCurrentDirectory() + "\\DefaultData\\DefaultQuestConfig.xml",
			FileMode.Open, FileAccess.Read, FileShare.Read);
		var questReader = XmlReader.Create(questStream, questReaderSettings);
		allQuests = GameDataManager.LoadQuests(questReader);
		questReader.Close();
	}
	public static void SetQuests(List<Quest> quests)
	{
		allQuests = new List<Quest>();
		allQuests.AddRange(quests);
	}
	public static void Update()
	{
		foreach (var quest in allQuests)
		{
			quest.CheckIfCompleted();
		}
	}

	public static List<Quest> GetQuestByQuestGiver(string questgiverName)
	{
		return allQuests.Where(quest => quest.questGiver == questgiverName).ToList();
	}
}
