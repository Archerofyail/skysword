﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using UnityEngine;
using UnityEngine.SceneManagement;
//To:Create a default quest save file. If there aren't any quests loaded into the game from the normal save file, get them from that instead
public static class GameDataManager
{
	public static int selectedSave = -1;
	private static List<SaveGame> SaveGames;
	public static Dictionary<string, Shop> shops { get; private set; }
	private static SaveGame LoadedGame;
	private const float CurrSaveVersion = 0.1f;

	static GameDataManager()
	{
		PlayerPrefs.DeleteAll();
		SaveGames = new List<SaveGame>();
		shops = new Dictionary<string, Shop>();
		LoadGames();

	}

	public static void SaveGame()
	{
		var saveDir = new DirectoryInfo(Directory.GetCurrentDirectory() + "\\SaveGames");
		if (!saveDir.Exists)
		{
			saveDir.Create();
		}
		FileStream saveStream;
		if (PlayerPrefs.HasKey("SaveFile"))
		{
			saveStream =
				new FileStream((PlayerPrefs.GetString("SaveFile")), FileMode.Create);
		}
		else
		{
			var saveString = Directory.GetCurrentDirectory() + "\\SaveGames\\" + "SaveGame" +
							 Directory.GetFiles(Directory.GetCurrentDirectory() + "\\SaveGames").Length + ".xml";
			saveStream =
				new FileStream((saveString), FileMode.Create);
			PlayerPrefs.SetString("SaveFile", saveString);
		}
		#region Collecting Data

		var pickups = GameObject.FindGameObjectsWithTag(Tags.Pickup);

		var commonPlayerData = GameObject.Find("Player").GetComponent<CommonPlayerData>();
		var playerTransform = commonPlayerData.transform;
		var Inventory = CommonPlayerData.inventory.InventoryItems;
		var Quests = QuestManager.allQuests;
		var equipped = commonPlayerData.GetEquipped();
		#endregion
		var settings = new XmlWriterSettings { NewLineOnAttributes = true, Indent = true, IndentChars = "\t" };
		using (var saveWriter = XmlWriter.Create(saveStream, settings))
		{
			saveWriter.WriteStartElement(XmlNames.SaveGame);
			saveWriter.WriteAttributeString(XmlNames.SceneNameAttribute, Application.loadedLevelName);
			saveWriter.WriteAttributeString(XmlNames.SaveVersion, CurrSaveVersion.ToString(CultureInfo.InvariantCulture));
			saveWriter.WriteStartElement(XmlNames.Player);
			saveWriter.WriteAttributeString(XmlNames.IsInAirAttribute, commonPlayerData.isFlyingScriptEnabled.ToString());
			SaveTransform(saveWriter, playerTransform);
			//Saving player inventory
			if (Inventory.Count > 0)
			{
				saveWriter.WriteStartElement(XmlNames.Inventory);
				foreach (var item in Inventory)
				{
					SaveItem(item, saveWriter);
				}
				saveWriter.WriteEndElement();
			}
			//Saving equipped items
			if (equipped.Count > 0)
			{
				saveWriter.WriteStartElement(XmlNames.Equipped);
				foreach (var item in equipped)
				{
					SaveItem(item, saveWriter);
				}
				saveWriter.WriteEndElement();
			}

			//Here ends the saving of the player
			saveWriter.WriteEndElement();
			//Saving shops
			if (shops.Keys.Count > 0 && shops.Any(shop => shop.Value.InventoryItems.Count > 0))
			{
				saveWriter.WriteStartElement(XmlNames.Shops);
				foreach (var shopInventory in shops.Keys)
				{
					saveWriter.WriteStartElement(XmlNames.Shop);
					saveWriter.WriteAttributeString(XmlNames.Name, shopInventory);
					foreach (var item in shops[shopInventory].InventoryItems)
					{
						SaveItem(item, saveWriter);
					}
					saveWriter.WriteEndElement();
				}
				//Ending saving shops
				saveWriter.WriteEndElement();
			}
			//Saving pickups
			if (pickups.Length > 0)
			{
				saveWriter.WriteStartElement(XmlNames.Pickups);
				foreach (var pickup in pickups)
				{
					saveWriter.WriteStartElement(XmlNames.Pickup);
					SaveTransform(saveWriter, pickup.transform);
					SaveItem(pickup.GetComponent<Pickup>().Item, saveWriter);
					saveWriter.WriteEndElement();
				}
				//Ending pickup saves
				saveWriter.WriteEndElement();
			}
			//Starting saving quests
			if (Quests.Count > 0)
			{
				saveWriter.WriteStartElement(XmlNames.Quests);
				foreach (var quest in Quests)
				{
					SaveQuest(saveWriter, quest);
				}
				saveWriter.WriteEndElement();
			}
			//Ending saving the file entirely
			saveWriter.WriteEndElement();


		}
		saveStream.Close();
		LoadGames();
	}

	private static void SaveTransform(XmlWriter saveWriter, Transform transformToSave)
	{
		saveWriter.WriteStartElement(XmlNames.Transform);
		saveWriter.WriteStartElement(XmlNames.Position);
		saveWriter.WriteAttributeString("x", transformToSave.position.x.ToString(CultureInfo.InvariantCulture));
		saveWriter.WriteAttributeString("y", transformToSave.position.y.ToString(CultureInfo.InvariantCulture));
		saveWriter.WriteAttributeString("z", transformToSave.position.z.ToString(CultureInfo.InvariantCulture));
		saveWriter.WriteEndElement();
		saveWriter.WriteStartElement(XmlNames.Rotation); //In Euler angles
		saveWriter.WriteAttributeString("x", transformToSave.rotation.x.ToString(CultureInfo.InvariantCulture));
		saveWriter.WriteAttributeString("y", transformToSave.rotation.y.ToString(CultureInfo.InvariantCulture));
		saveWriter.WriteAttributeString("z", transformToSave.rotation.z.ToString(CultureInfo.InvariantCulture));
		saveWriter.WriteEndElement();
		saveWriter.WriteEndElement();
	}

	private static void SaveQuest(XmlWriter saveWriter, Quest questToSave)
	{
		saveWriter.WriteStartElement(XmlNames.Quest);
		saveWriter.WriteAttributeString(XmlNames.QuestName, questToSave.questName);
		saveWriter.WriteAttributeString(XmlNames.QuestGiver, questToSave.questGiver);
		saveWriter.WriteAttributeString(XmlNames.QuestDescription, questToSave.questDescription);
		saveWriter.WriteAttributeString(XmlNames.QuestIsAvailable, questToSave.isAvailable.ToString());
		saveWriter.WriteAttributeString(XmlNames.SkillPointsGained, questToSave.skillPointsGained.ToString(CultureInfo.InvariantCulture));
		saveWriter.WriteAttributeString(XmlNames.QuestStatus, questToSave.questStatus.ToString());
		saveWriter.WriteStartElement(XmlNames.QuestObjectives);
		foreach (var objective in questToSave.questObjectives)
		{
			saveWriter.WriteStartElement(XmlNames.QuestObjective);
			saveWriter.WriteAttributeString(XmlNames.QuestObjectiveIsComplete, objective.isCompleted.ToString());
			saveWriter.WriteAttributeString(XmlNames.QuestObjectiveTargetName, objective.targetName);
			saveWriter.WriteAttributeString(XmlNames.QuestObjectiveTargetSender, objective.targetSender);
			saveWriter.WriteAttributeString(XmlNames.QuestObjectiveDescription, objective.objectiveDescription);
			saveWriter.WriteAttributeString(XmlNames.QuestObjectiveAmountToComplete, objective.amountToComplete.ToString(CultureInfo.InvariantCulture));
			saveWriter.WriteAttributeString(XmlNames.QuestObjectiveAmountCompleted, objective.amountCompleted.ToString(CultureInfo.InvariantCulture));
			saveWriter.WriteAttributeString(XmlNames.QuestObjectiveIsVisible, objective.isVisible.ToString());
			saveWriter.WriteStartElement(XmlNames.QuestObjectiveWorldPosition);
			saveWriter.WriteAttributeString("x", objective.worldPositon.x.ToString(CultureInfo.InvariantCulture));
			saveWriter.WriteAttributeString("y", objective.worldPositon.y.ToString(CultureInfo.InvariantCulture));
			saveWriter.WriteAttributeString("z", objective.worldPositon.z.ToString(CultureInfo.InvariantCulture));
			saveWriter.WriteEndElement();
			saveWriter.WriteEndElement();
		}
		saveWriter.WriteEndElement();
		saveWriter.WriteEndElement();
	}

	private static void SaveItem(Item item, XmlWriter saveWriter)
	{
		if (item != null)
		{
			saveWriter.WriteStartElement(XmlNames.Item);
			saveWriter.WriteAttributeString(XmlNames.Type, item.GetType().ToString());
			saveWriter.WriteAttributeString(XmlNames.Name, item.ItemName);
			saveWriter.WriteElementString(XmlNames.Type, item.typeInt.ToString(CultureInfo.InvariantCulture));
			saveWriter.WriteStartElement(XmlNames.Stats);
			foreach (var stat in item.Stats)
			{
				saveWriter.WriteStartElement(XmlNames.Stat);
				saveWriter.WriteAttributeString(XmlNames.Name, stat.Key);
				saveWriter.WriteAttributeString(XmlNames.StatValueAttribute, stat.Value.ToString(CultureInfo.InvariantCulture));
				saveWriter.WriteEndElement();
			}
			saveWriter.WriteEndElement();
			saveWriter.WriteEndElement();
		}
	}

	private static void LoadGames()
	{
		SaveGames = new List<SaveGame>();
		var folderPath = Directory.GetCurrentDirectory() + "\\SaveGames";
		var dirInfo = new DirectoryInfo(folderPath);
		if (!dirInfo.Exists)
		{
			dirInfo.Create();
		}
		if (Directory.GetFiles(folderPath).Any())
		{
			foreach (var file in Directory.GetFiles(folderPath))
			{
				bool hasQuests = false;
				bool hasSkills = false;
				float saveVersion = 0;
				if (file.EndsWith("xml"))
				{
					var saveGame = new SaveGame();
					var saveGameReaderSettings = new XmlReaderSettings { CloseInput = true };
					var saveGameStream = new FileStream(file, FileMode.Open);
					using (var saveReader = XmlReader.Create(saveGameStream, saveGameReaderSettings))
					{
						while (saveReader.Read())
						{
							if (saveReader.IsStartElement())
							{
								switch (saveReader.Name)
								{
									case XmlNames.SaveGame:
									{
										saveGame.sceneName = saveReader.GetAttribute(XmlNames.SceneNameAttribute);
										if (!float.TryParse(saveReader.GetAttribute(XmlNames.SaveVersion), out saveVersion))
										{
											saveVersion = 0;
										}
									}
									break;
									case XmlNames.Player:
									{
										saveGame.isInAir = saveReader.GetAttribute(XmlNames.IsInAirAttribute);

										var playerTrans = LoadTransform(saveReader);
										saveGame.playerPos = playerTrans.position;
										saveGame.playerRot = playerTrans.eulerRotation;
									}
									break;
									case XmlNames.Inventory:
									{
										if (saveReader.ReadToDescendant(XmlNames.Item))
										{
											bool isReadingNext;
											do
											{
												saveGame.Items.Add(LoadItem(saveReader));
												isReadingNext = saveReader.ReadToNextSibling(XmlNames.Item);
											} while (isReadingNext);
										}

									}
									break;
									case XmlNames.Equipped:
									{
										if (saveReader.ReadToDescendant(XmlNames.Item))
										{
											do
											{
												saveGame.Equipped.Add(LoadItem(saveReader));
											} while (saveReader.ReadToNextSibling(XmlNames.Item));
										}
									}
									break;
									case XmlNames.Shops:
									{
										if (saveReader.ReadToDescendant(XmlNames.Shop))
										{
											do
											{
												LoadShop(saveReader, saveGame);
											} while (saveReader.ReadToNextSibling(XmlNames.Shop));
										}
									}
									break;
									case XmlNames.Pickups:
									{
										if (saveReader.ReadToDescendant(XmlNames.Pickup))
										{
											do
											{
												LoadPickup(saveReader, saveGame);
												saveReader.ReadToNextSibling(XmlNames.Item);
											} while (saveReader.ReadToNextSibling(XmlNames.Pickup));
										}
									}
									break;
									case XmlNames.Quests:
									{
										hasQuests = true;
										var questReaderSettings = new XmlReaderSettings { CloseInput = true };
										var questReader =
											XmlReader.Create(new FileStream(Directory.GetCurrentDirectory() + "\\DefaultData\\DefaultQuestConfig.xml",
												FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read), questReaderSettings);
										questReader.Close();
										float defaultQuestConfigVers;
										if (float.TryParse(questReader.GetAttribute(XmlNames.SaveVersion), out defaultQuestConfigVers))
										{
											saveGame.quests.AddRange(defaultQuestConfigVers > saveVersion
												? LoadQuests(questReader)
												: LoadQuests(saveReader));
										}
										else
										{
											saveGame.quests.AddRange(LoadQuests(saveReader));
										}
									}
									break;
									case "Skills":
									{
										hasSkills = true;
										saveGame.allSkills.AddRange(LoadAllSkills(saveReader));
									}
									break;
									default:
									{
									}
									break;
								}
							}
						}
						if (!hasQuests)
						{
							var questReaderSettings = new XmlReaderSettings { CloseInput = true };
							var questReader =
								XmlReader.Create(new FileStream(Directory.GetCurrentDirectory() + "\\DefaultData\\DefaultQuestConfig.xml",
									FileMode.Open, FileAccess.Read, FileShare.Read), questReaderSettings);
							saveGame.quests.AddRange(LoadQuests(questReader));
							questReader.Close();
							QuestManager.SetQuests(saveGame.quests);
						}
						if (!hasSkills)
						{
							var skillReaderSettings = new XmlReaderSettings {CloseInput = true};
							var skillReader =
								XmlReader.Create(
									new FileStream(Directory.GetCurrentDirectory() + "\\DefaultData\\Skills.xml", FileMode.Open, FileAccess.Read,
										FileShare.Read), skillReaderSettings);
							saveGame.allSkills.AddRange(LoadAllSkills(skillReader));
							skillReader.Close();
							SkillManager.SetSkillTrees(saveGame.allSkills);
						}
					}

					saveGame.saveFileName = file;
					SaveGames.Add(saveGame);
					saveGameStream.Close();
				}
			}
		}
	}

	private static void LoadPickup(XmlReader saveReader, SaveGame saveGame)
	{
		var pickupObject = LoadTransform(saveReader);
		saveReader.ReadToNextSibling(XmlNames.Item);
		pickupObject.pickup = LoadItem(saveReader);
		saveGame.pickupObjects.Add(pickupObject);
	}

	private static PickupHolder LoadTransform(XmlReader saveReader)
	{
		saveReader.ReadToDescendant(XmlNames.Transform);
		var transContainer = new PickupHolder();
		saveReader.ReadToDescendant(XmlNames.Position);
		transContainer.position = new Vector3(Convert.ToSingle(saveReader.GetAttribute("x")),
									   Convert.ToSingle(saveReader.GetAttribute("y")), Convert.ToSingle(saveReader.GetAttribute("z")));
		saveReader.ReadToNextSibling(XmlNames.Rotation);
		transContainer.eulerRotation = new Vector3(Convert.ToSingle(saveReader.GetAttribute("x")),
										Convert.ToSingle(saveReader.GetAttribute("y")), Convert.ToSingle(saveReader.GetAttribute("z")));
		saveReader.ReadToNextSibling(XmlNames.Scale);
		return transContainer;
	}

	private static void LoadShop(XmlReader saveReader, SaveGame saveGame)
	{
		string shopName = saveReader.GetAttribute(XmlNames.Name);
		var itemsFromShop = new List<Item>();

		if (saveReader.ReadToDescendant(XmlNames.Item))
		{
			do
			{
				var addedItem = LoadItem(saveReader);
				itemsFromShop.Add(addedItem);
			} while (saveReader.ReadToNextSibling(XmlNames.Item));
		}
		if (shopName != null)
		{
			saveGame.Shops.Add(shopName, new Shop(itemsFromShop));
		}
	}

	private static Item LoadItem(XmlReader saveReader)
	{
		try
		{
			string typeName = saveReader.GetAttribute(XmlNames.Type);
			var itemName = saveReader.GetAttribute(XmlNames.Name);
			saveReader.ReadToDescendant(XmlNames.Type);
			var typeInt = saveReader.ReadElementContentAsInt();
			var loadedItem = (Item)Activator.CreateInstance(Type.GetType(typeName));
			loadedItem.ItemName = itemName;
			loadedItem.typeInt = typeInt;
			saveReader.ReadToNextSibling(XmlNames.Stats);
			if (saveReader.ReadToDescendant(XmlNames.Stat))
			{
				do
				{
					loadedItem.Stats.Add(saveReader.GetAttribute(XmlNames.Name), Convert.ToInt32(saveReader.GetAttribute(XmlNames.StatValueAttribute)));
				} while (saveReader.ReadToNextSibling(XmlNames.Stat));
			}
			if (saveReader.Name == XmlNames.Stats)
			{
				saveReader.ReadToNextSibling(XmlNames.Stats);
			}
			return loadedItem;
		}
		catch (Exception e)
		{
			Debug.Log("ERROR: " + e.Message);
		}
		return null;
	}

	public static List<Quest> LoadQuests(XmlReader saveReader)
	{
		var loadedQuests = new List<Quest>();
		if (saveReader.ReadToDescendant(XmlNames.Quest))
		{
			do
			{
				loadedQuests.Add(LoadQuest(saveReader));
			} while (saveReader.ReadToNextSibling(XmlNames.Quest));
		}
		return loadedQuests;
	}

	public static Quest LoadQuest(XmlReader questReader)
	{
		var quest = new Quest
		{
			questName = questReader.GetAttribute(XmlNames.QuestName),
			questGiver = questReader.GetAttribute(XmlNames.QuestGiver),
			questDescription = questReader.GetAttribute(XmlNames.QuestDescription)
		};
		try
		{

			quest.isAvailable = Convert.ToBoolean(questReader.GetAttribute(XmlNames.QuestIsAvailable));


		}
		catch (FormatException)
		{
			Debug.Log("Error, IsAvailable attribute failed to convert in quest " + quest.questName + ", using default value true");
			quest.isAvailable = true;
		}
		try
		{
			quest.skillPointsGained = Convert.ToInt32(questReader.GetAttribute(XmlNames.SkillPointsGained));
		}
		catch (Exception)
		{
			Debug.Log("Error failed to convert SkillPoints gained attribute in quest " + quest.questName + " using default value 1");
		}

		try
		{
			quest.questStatus = (QuestStatus)Enum.Parse(typeof(QuestStatus), questReader.GetAttribute(XmlNames.QuestStatus));
		}
		catch (ArgumentNullException)
		{
			Debug.Log("Error quest status for " + quest.questName + " was null, using default NotAccepted");
			quest.questStatus = QuestStatus.NotAccepted;
		}
		catch (ArgumentException)
		{
			Debug.Log("Error quest status for " + quest.questName + " was a non-existant status, using default NotAccepted");
			quest.questStatus = QuestStatus.NotAccepted;
		}
		if (questReader.ReadToDescendant(XmlNames.QuestObjectives))
		{
			if (questReader.ReadToDescendant(XmlNames.QuestObjective))
			{
				var questObjectives = new List<QuestObjective>();
				do
				{
					var objective = new QuestObjective();
					try
					{
						objective.isCompleted = Convert.ToBoolean(questReader.GetAttribute(XmlNames.QuestObjectiveIsComplete));
					}
					catch (FormatException)
					{
						Debug.Log("Failed to convert IsCompleted attribute in objective in quest " + quest.questName + " using default value false");
						throw;
					}
					objective.targetName = questReader.GetAttribute(XmlNames.QuestObjectiveTargetName);
					objective.targetSender = questReader.GetAttribute(XmlNames.QuestObjectiveTargetSender);
					objective.objectiveDescription = questReader.GetAttribute(XmlNames.QuestObjectiveDescription);
					try
					{
						objective.amountToComplete = Convert.ToInt32(questReader.GetAttribute(XmlNames.QuestObjectiveAmountToComplete));
						objective.amountCompleted = Convert.ToInt32(questReader.GetAttribute(XmlNames.QuestObjectiveAmountCompleted));
					}
					catch (FormatException)
					{
						Debug.Log("Failed to convert Either AmountCompleted or AmountToComplete Attributes, setting them to default value, 0 and 1");
						objective.amountCompleted = 0;
						objective.amountToComplete = 1;
					}
					try
					{
						objective.isVisible = Convert.ToBoolean(questReader.GetAttribute(XmlNames.QuestObjectiveIsVisible));
					}
					catch (FormatException)
					{
						Debug.Log("Failed to convert IsVisible attribute to boolean, setting to default value true");
						objective.isVisible = true;
					}
					if (questReader.ReadToDescendant(XmlNames.QuestObjectiveWorldPosition))
					{
						objective.worldPositon = new Vector3(Convert.ToSingle(questReader.GetAttribute("x")),
							Convert.ToSingle(questReader.GetAttribute("y")), Convert.ToSingle(questReader.GetAttribute("z")));
					}
					questObjectives.Add(objective);
				} while (questReader.ReadToNextSibling(XmlNames.QuestObjective));
				questReader.ReadToDescendant(XmlNames.QuestObjectives);
				quest.questObjectives.AddRange(questObjectives);
			}
		}
		questReader.ReadToNextSibling(XmlNames.QuestObjective);

		questReader.ReadToNextSibling(XmlNames.QuestObjectives);
		return quest;
	}

	public static IEnumerable<SkillTree> LoadAllSkills(XmlReader skillReader)
	{
		var allSkills = new List<SkillTree>();
		if (skillReader.ReadToDescendant("SkillTree"))
		{
			do
			{
				allSkills.Add(LoadSkillTree(skillReader));
			} while (skillReader.ReadToNextSibling("SkillTree"));

		}
		return allSkills;
	}

	private static SkillTree LoadSkillTree(XmlReader treeReader)
	{
		var tree = new SkillTree { treeName = treeReader.GetAttribute("Name") };
		if (treeReader.ReadToDescendant("SkillTier"))
		{
			do
			{
				tree.skills.Add(LoadSkillTier(treeReader));
			} while (treeReader.ReadToNextSibling("SkillTier"));
			
		}
		tree.SumPointsInTiers();
		return tree;
	}

	private static List<Skill> LoadSkillTier(XmlReader tierReader)
	{
		var skillTier = new List<Skill>();
		int tier;
		try
		{
			tier = Convert.ToInt32(tierReader.GetAttribute("Tier"));
		}
		catch (Exception)
		{
			tier = 0;
		}

		if (tierReader.ReadToDescendant("Skill"))
		{
			do
			{
				skillTier.Add(LoadSkill(tierReader, tier));
			} while (tierReader.ReadToNextSibling("Skill"));

		}
		return skillTier;
	}

	private static Skill LoadSkill(XmlReader skillReader, int tier)
	{
		var skill = new Skill
		{
			skillName = skillReader.GetAttribute("SkillName"),
			skillDescription = skillReader.GetAttribute("SkillDescription"),
			skillTex = (Texture2D) Resources.Load(skillReader.GetAttribute("SkillTexName")),
			tier = tier
		};
		try
		{
			skill.rank = Convert.ToInt32(skillReader.GetAttribute("SkillRank"));
		}
		catch (Exception)
		{
			Debug.Log("Error rank not found, using default of 0");
			skill.rank = 0;
		}
		if (skillReader.ReadToDescendant("DependentSkills"))
		{
			if (skillReader.ReadToDescendant("SkillName"))
			{
				do
				{
					skill.dependentSkills.Add(skillReader.ReadElementContentAsString());
				} while (skillReader.ReadToNextSibling("SkillName"));
			}
			skillReader.ReadToNextSibling("Stuff");
		}
		return skill;
	}

	public static void LoadGame(int counter)
	{
		selectedSave = counter;
		LoadedGame = SaveGames[counter];
		PlayerPrefs.DeleteAll();
		PlayerPrefs.SetString("SaveFile", LoadedGame.saveFileName);
		PlayerPrefs.Save();
		QuestManager.SetQuests(LoadedGame.quests);
		SkillManager.SetSkillTrees(LoadedGame.allSkills);
		Time.timeScale = 1;
		SceneManager.LoadScene(LoadedGame.sceneName);
	}

	public static void DeleteGame(int selectedGame)
	{
		File.Delete(SaveGames[selectedGame].saveFileName);
		SaveGames.RemoveAt(selectedGame);
	}

	public static IEnumerable<SaveGame> GetGames()
	{
		return SaveGames;
	}

	public static SaveGame GetLoadedGame()
	{
		if (LoadedGame != null)
		{
			return LoadedGame;
		}
		if (selectedSave != -1)
		{
			return SaveGames[selectedSave];
		}
		return null;
	}

	public static void AddShop(string shopName, Shop shop)
	{
		if (shops.ContainsKey(shopName))
		{
			shops.Remove(shopName);
		}
		shops.Add(shopName, shop);
	}
}

[Serializable]
public class PickupHolder
{
	public Vector3 position;
	public Vector3 eulerRotation;
	public Item pickup;

	public PickupHolder(Vector3 pos, Vector3 eulerRot, Item pick)
	{
		position = pos;
		eulerRotation = eulerRot;
		pickup = pick;
	}

	public PickupHolder()
	{
		position = new Vector3();
		eulerRotation = new Vector3();
		pickup = new Item();
	}
}

[Serializable]
public class SaveGame
{
	public Vector3 playerPos;
	public Vector3 playerRot;
	public string sceneName;
	public string saveFileName;
	public string isInAir;
	public List<Item> Items;
	public List<Item> Equipped;
	public Dictionary<string, Shop> Shops;
	public List<PickupHolder> pickupObjects;
	public List<Quest> quests;
	public List<SkillTree> allSkills; 

	public SaveGame(Vector3 playerPos, string sceneName, string saveFileName, string isInAir)
	{
		this.playerPos = playerPos;
		this.sceneName = sceneName;
		this.saveFileName = saveFileName;
		this.isInAir = isInAir;
	}

	public SaveGame()
	{
		Items = new List<Item>(2);
		Equipped = new List<Item>(5);
		Shops = new Dictionary<string, Shop>();
		pickupObjects = new List<PickupHolder>();
		quests = new List<Quest>();
		allSkills = new List<SkillTree>();
	}
}
