﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public static class SkillManager
{

	public static List<SkillTree> SkillTrees { get; private set; }

	static SkillManager()
	{
		SkillTrees = new List<SkillTree>();
	}

	public static void SetSkillTrees(IEnumerable<SkillTree> skillTrees)
	{
		SkillTrees = new List<SkillTree>(skillTrees);
	}
}
