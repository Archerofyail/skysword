﻿using UnityEngine;

public class ManagersUpdater : MonoBehaviour
{

	void Awake()
	{
		QuestManager.Awake();
	}
	void Update()
	{
		MenuManager.Update();
		QuestManager.Update();
	}
}
