﻿using System;
using UnityEngine;
using System.Collections;

/// <summary>
/// Manages and displays the current remote purchase
/// </summary>
public class RemotePurchaseTracker : MonoBehaviour
{
	//ToDo: Replace this with a queue of updates. There will be a Dictionary of strings and each one will will slowly fade unless it gets updates
	private string itemName = "";
	private float secondsRemaining;
	public float arrivedTextTimeOut = 3f;
	private bool itemArrivedTextIsShowing;
	private Color arrivedTextColor;
	public GUISkin skin;
	void OnGUI()
	{
		if (skin)
		{
			GUI.skin = skin;
		}
		GUILayout.BeginArea(new Rect(Screen.width * 0.8f, Screen.height * 0.25f, Screen.width * 0.2f, Screen.height * 0.1f));
		if (itemName != "")
		{
			GUILayout.Label("Remote Purchase: " + itemName + "\nTime Remaining: " + secondsRemaining + "s");
		}
		else if (itemArrivedTextIsShowing)
		{
			GUI.color = arrivedTextColor;
			GUILayout.Label("Item Has Arrived!");
			GUI.color = Color.white;
		}
		GUILayout.EndArea();
	}

	IEnumerator PurchaseTimer(RemotePurchase purchase)
	{
		itemName = purchase.purchasedItem.ItemName;
		float startTime = Time.time;
		float timer = 0;
		while (timer < startTime + purchase.secondsToWait)
		{
			timer += Time.deltaTime;
			secondsRemaining = purchase.secondsToWait - timer;
			yield return null;
		}
		CommonPlayerData.inventory.Add(purchase.purchasedItem);
		itemName = "";
	}

	IEnumerator ItemArrivedTimer()
	{
		float timer = 0;
		itemArrivedTextIsShowing = true;
		while (timer < arrivedTextTimeOut)
		{
			timer += Time.deltaTime;
			arrivedTextColor = new Color(arrivedTextColor.r, arrivedTextColor.g, arrivedTextColor.b, (timer/arrivedTextTimeOut));
			yield return null;
		}
		itemArrivedTextIsShowing = false;
	}
}

public class RemotePurchase
{
	public Item purchasedItem { get; private set; }
	public float secondsToWait { get; private set; }

	public RemotePurchase(Item itemPurchased, float waitTime)
	{
		purchasedItem = itemPurchased;
		secondsToWait = waitTime;
	}
}
