﻿using System;
using UnityEngine;
using System.Collections;

/// <summary>
/// Holds the currently equipped weapon and manages equipping and attacking
/// </summary>
public class WeaponContainer : MonoBehaviour
{
	public bool isAttacking { get; set; }
	private bool doneFirstAttack;
	private bool doneSecondAttack;
	private bool doneThirdAttack;
	[HideInInspector]
	public float lastAttackTime;
	public float timeBeforeAttackReset = 0.7f;
	[SerializeField]
	private float
			delayBetweenAttacks = 0.4f;
	private Animation ParentAnimation;
	[SerializeField]
	public Weapon EquippedWeapon;

	void Start()
	{
		ParentAnimation = transform.parent.GetComponent<Animation>();
		EquippedWeapon = null;
	}


	void Update()
	{

	}

	public void Attack()
	{
		print("Should attack if weapon exists");
		if (transform.parent.Find("PlayerModel/Sword"))
		{
			print("Found the sword");
			isAttacking = true;
			if (!doneFirstAttack && !ParentAnimation["RightToLeftSwordSwing"].enabled)
			{

				ParentAnimation.Play("LeftToRightSwordSwing");
				print("Entered first attack");
				transform.parent.GetComponent<Rigidbody>().AddRelativeForce(0, 0, 100);
				doneFirstAttack = true;
				doneSecondAttack = false;
				lastAttackTime = Time.time;
			}
			else if (doneFirstAttack && !doneSecondAttack && Time.time > lastAttackTime + delayBetweenAttacks)
			{
				print("Entered second attack");

				ParentAnimation.Blend("RightToLeftSwordSwing");
				transform.parent.GetComponent<Rigidbody>().AddRelativeForce(0, 0, 100);
				doneSecondAttack = true;
				lastAttackTime = Time.time;
			}
			else if (doneSecondAttack && !doneThirdAttack && Time.time > lastAttackTime + delayBetweenAttacks)
			{
				ParentAnimation.Blend("LeftToRightSlash");
				print("Entered third attack");

				transform.parent.GetComponent<Rigidbody>().AddRelativeForce(0, 0, 100);
				doneThirdAttack = true;
				lastAttackTime = Time.time;
			}
		}
	}

	public void CheckForReset()
	{
		if (Time.time - lastAttackTime > timeBeforeAttackReset)
		{
			isAttacking = false;
			if (doneFirstAttack && !doneSecondAttack)
			{
				ParentAnimation.Blend("ReturnToDefaultSwordPosFromRight");
				doneFirstAttack = false;
			}
			else if (doneSecondAttack && !doneThirdAttack)
			{
				ParentAnimation.Blend("ReturnToDefaultSwordPosFromLeft");
				doneFirstAttack = false;
				doneSecondAttack = false;
			}
			else if (doneThirdAttack)
			{
				doneFirstAttack = false;
				doneSecondAttack = false;
				doneThirdAttack = false;
				ParentAnimation.Blend("ReturnToDefaultSwordPosFromRight");
			}
		}
	}

	public void EquipWeapon(Weapon weaponToEquip)
	{
		var doesExist = transform.parent.Find("PlayerModel/Sword");
		if (doesExist)
		{
			Destroy(doesExist.gameObject);
		}
		if (EquippedWeapon != null)
		{
			CommonPlayerData.inventory.Add(EquippedWeapon);
		}
		weaponToEquip.itemPrefab = (GameObject) Resources.Load("Prefabs/" + weaponToEquip.WeaponType);
		EquippedWeapon = weaponToEquip;
		if (weaponToEquip.itemPrefab)
		{
			switch (weaponToEquip.WeaponType)
			{
				case WeaponType.OneHSword:
				{
					var sword =
						(GameObject) Instantiate(weaponToEquip.itemPrefab, transform.position - (Vector3.right*0.5f), transform.rotation);
					sword.transform.parent = transform.parent.Find("PlayerModel");
					sword.name = "Sword";
					var sword2 =
						(GameObject) Instantiate(weaponToEquip.itemPrefab, transform.position + (Vector3.right*0.5f), transform.rotation);
					sword2.transform.parent = transform.parent.Find("PlayerModel"); 
					sword2.name = "Sword";
					break;
				}
				case WeaponType.TwoHSword:
				{
					var THSword = (GameObject) Instantiate(weaponToEquip.itemPrefab, transform.position, transform.rotation);
					THSword.transform.parent = transform.parent.Find("PlayerModel");
					THSword.name = "Sword";
					break;
				}
				case WeaponType.DoubleBladedSword:
				{
					var DBSword = (GameObject) Instantiate(weaponToEquip.itemPrefab, transform.position, transform.rotation);
					DBSword.transform.parent = transform.parent.Find("PlayerModel");
					DBSword.name = "Sword";
					break;
				}
				case WeaponType.ChainScythe:
				{
					var scythe = (GameObject) Instantiate(weaponToEquip.itemPrefab, transform.position, transform.rotation);
					scythe.transform.parent = transform.parent.Find("PlayerModel");
					scythe.name = "Sword";
					break;
				}
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}
