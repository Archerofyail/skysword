﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Manages Flight-specific player movement
/// </summary>
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(PlayerGroundMovement))]
[RequireComponent(typeof(CommonPlayerData))]
public class PlayerFlyingMovement : MonoBehaviour
{
	#region Movement

	[SerializeField]
	private float boostMovementForce = 2000f;

	[SerializeField]
	private float defaultMovementForce = 1000f;

	[SerializeField]
	private float heightIncreaseForce = 300;
	#endregion

	#region Camera
	public float minCameraDist = -6;
	public float boostCameraDistMax = -10;
	public float cameraChangeSpeed = 6f;
	#endregion

	public CommonPlayerData commonItems;
	private bool isAbleToBoost;
	private Vector3 relVelocity;

	#region AnimationTimings

	private bool swordIdleHasPlayed;

	private float swordIdleStartTime;
	#endregion

	#region SwordPositions

	#endregion
	#region Combat Fields

	[SerializeField]
	private float lastAttackTime;
	[SerializeField]
	private bool doneFirstAttack;
	[SerializeField]
	private float timeBeforeAttackReset = 1f;
	private float dashTime;
	[SerializeField]
	private float dashTimeMax = 2f;
	public bool isDashAttacking { get; private set; }
	private bool isDashCooling;
	public bool isAttacking { get; private set; }
	#endregion

	void OnEnable()
	{
		GetComponent<Rigidbody>().useGravity = false;
		commonItems.playerModel.transform.rotation = transform.rotation;
		commonItems.mainCamera.transform.localRotation = Quaternion.Euler(8.5f, 0, 0);
		StartCoroutine("FlyingMovement");
	}

	void OnDisable()
	{
		StopCoroutine("FlyingMovement");
	}

	void Update()
	{
		

		#region Movement


		if (Physics.Raycast(transform.position, Vector3.down, 1, 1 << 8))
		{
			commonItems.AirToGround();
		}

		//If you're speed is less than 0.7f
		if (Mathf.Abs(GetComponent<Rigidbody>().velocity.magnitude) < 0.7f)
		{
			//And you're not already playing the animation
			if (!GetComponent<Animation>()["FlyingHover"].enabled)
			{
				//Play it
				GetComponent<Animation>().Play("FlyingHover");
			}
		}
		//otherwise if you are going faster than .7f and the animation is playing
		else if (GetComponent<Animation>()["FlyingHover"].enabled)
		{
			//stop it
			GetComponent<Animation>().Stop("FlyingHover");
		}


		#endregion

		#region Combat
		if (Input.GetMouseButtonDown(1) && !Input.GetKey(KeyCode.LeftShift))
		{
			commonItems.StartCoroutine("MakePlayerFaceCamera");
			if (!doneFirstAttack)
			{
				isAttacking = true;
				GetComponent<Animation>().Play("360SpinAttack");
				GetComponent<Rigidbody>().AddRelativeForce(0, 0, 100);
				doneFirstAttack = true;
				lastAttackTime = Time.time;
			}
		}
		else
		{
			if (Time.time - lastAttackTime > timeBeforeAttackReset)
			{
				if (doneFirstAttack)
				{
					doneFirstAttack = false;
				}
			}
		}

		if (Input.GetKeyDown(KeyCode.Q) && !isDashCooling)
		{
			isDashAttacking = true;
		}


		if (isDashAttacking)
		{
			isAttacking = true;
			if (!commonItems.isMakingPlayerFaceCam)
			{
				GetComponent<Rigidbody>().AddRelativeForce(0, 0, 300, ForceMode.Acceleration);
			}
			dashTime += Time.deltaTime;
			if (dashTime > dashTimeMax)
			{
				dashTime = 0;
				isAttacking = false;
				isDashAttacking = false;
				StartCoroutine("DashCoolDown");
			}
		}
		#endregion

		#region KeyInputs

		if (Input.GetButton(InputNames.Jump))
		{
			GetComponent<Rigidbody>().AddForce(0, heightIncreaseForce, 0);
		}

		if (Input.GetButton(InputNames.DecreaseHeight))
		{
			GetComponent<Rigidbody>().AddForce(0, -heightIncreaseForce, 0);
		}

		if (Input.GetButtonDown(InputNames.DisableFlight))
		{
			commonItems.AirToGround();
		}
		#endregion
	}

	void OnCollisionEnter(Collision other)
	{
		//if (other.gameObject.tag == "Ground" && enabled)
		{
			commonItems.AirToGround();
		}
	}

	IEnumerator DashCoolDown()
	{
		float dashCoolTime = 0;
		const float dashCoolTimeMax = 2f;
		while (dashCoolTime < dashCoolTimeMax)
		{
			isDashCooling = true;
			dashCoolTime += Time.deltaTime;
			yield return null;
		}
		isDashCooling = false;
	}

	/// <summary>
	/// Controls the camera and player model during flight, and controls when boosting happens
	/// </summary>
	/// <returns></returns>
	IEnumerator FlyingMovement()
	{
		while (true)
		{
			if (Input.GetKeyDown(KeyCode.LeftShift) || commonItems.pressedShiftInGroundScript)
			{
				if (Input.GetAxis(InputNames.ForwardBackward) > 0)
				{
					isAbleToBoost = true;

					commonItems.playerModel.transform.localRotation = Quaternion.Euler(transform.localRotation.x + 90, 0,
						(relVelocity.x / 10) * -15);
					if (!GetComponent<Animation>()["BoostStart"].enabled)
					{
						GetComponent<Animation>().Play("BoostStart");
					}
					commonItems.pressedShiftInGroundScript = false;
				}
			}
			if (Input.GetKey(KeyCode.LeftShift))
			{
				if (isAbleToBoost)
				{
					commonItems.movementForce = boostMovementForce;
					commonItems.maxVelocity = 10;
					if (commonItems.mainCamera.transform.localPosition.z > boostCameraDistMax)
					{
						commonItems.mainCamera.transform.Translate(0, 0, -cameraChangeSpeed * Time.deltaTime);
					}
					commonItems.playerModel.transform.localRotation = Quaternion.Euler(transform.localRotation.x + 90, 0,
						(relVelocity.x / 10) * -30);
				}
			}
			else
			{
				isAbleToBoost = false;
				commonItems.movementForce = defaultMovementForce;
				commonItems.maxVelocity = 5;
				if (commonItems.mainCamera.transform.localPosition.z < minCameraDist)
				{
					commonItems.mainCamera.transform.Translate(0, 0, cameraChangeSpeed * Time.deltaTime);

				}
				commonItems.playerModel.transform.localRotation = Quaternion.Euler(
					transform.rotation.x + Mathf.Clamp((transform.InverseTransformDirection(GetComponent<Rigidbody>().velocity).z / 10) * 30, -90, 90),
					0, (transform.InverseTransformDirection(GetComponent<Rigidbody>().velocity).x / 10) * -30);
			}
			yield return null;
		}
	}
}
