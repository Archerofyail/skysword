﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

/// <summary>
/// The class that manages the equipped armor
/// </summary>
public class ArmourContainer : MonoBehaviour
{

	public Dictionary<string, Transform> armourPoints;
	public Dictionary<string, Armour> equippedArmour;

#if UNITY_EDITOR
	public List<Armour> equippedArmourList = new List<Armour>();
#endif

	void Start()
	{
		armourPoints = new Dictionary<string, Transform>();
		armourPoints.Add(transform.GetChild(0).name, transform.GetChild(0));
		armourPoints.Add(transform.GetChild(1).name, transform.GetChild(1));
		armourPoints.Add(transform.GetChild(2).name, transform.GetChild(2));
		armourPoints.Add(transform.GetChild(3).name, transform.GetChild(3));
		armourPoints.Add(transform.GetChild(4).name, transform.GetChild(4));
		equippedArmour = new Dictionary<string, Armour>();
	}


	void Update()
	{

	}

	public void EquipArmor(Armour armourItem)
	{
		if (equippedArmour.ContainsKey(armourItem.ArmourType.ToString()))
		{
			CommonPlayerData.inventory.Add(equippedArmour[armourItem.ArmourType.ToString()]);
		}
#if UNITY_EDITOR
		equippedArmourList.Add(armourItem);
#endif
		if (equippedArmour.ContainsKey(armourItem.ArmourType.ToString()))
		{
			equippedArmour.Remove(armourItem.ArmourType.ToString());
		}

		equippedArmour.Add(armourItem.ArmourType.ToString(), armourItem);
		if (armourItem.itemPrefab)
		{


			var armourGObject =
				(GameObject)
					Instantiate(armourItem.itemPrefab, armourPoints[armourItem.ArmourType.ToString()].position,
						armourPoints[armourItem.ArmourType.ToString()].rotation);
				armourGObject.transform.parent = armourPoints[armourItem.ArmourType.ToString()];
		}
	}
}
