﻿using System.Globalization;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/// <summary>
/// Manages and displays the health of the player
/// </summary>
public class PlayerDamageTracker : MonoBehaviour
{
	private static int Health = 100;
	public static int MaxHealth = 100;
	private bool isDead;
	public DamageText damageText;
	public CommonPlayerData commonPlayerData;
	public GUIStyle style;
	public GUIStyle ButtonStyle;
	
	void Update()
	{
		Health = Mathf.Clamp(Health, -1, MaxHealth);
	}

	/// <summary>
	/// Applies damage to the player and adds a damage text item to the list
	/// </summary>
	/// <param name="baseDamage"></param>
	public void DoDamage(int baseDamage)
	{
		Health -= baseDamage;
		damageText.AddText(baseDamage.ToString(CultureInfo.InvariantCulture),
			transform.position + (Vector3.up*Random.Range(0.5f, 1.5f)) + (Vector3.right*Random.Range(-3, 0.5f)));
		if (Health <= 0)
		{
			isDead = true;
			Time.timeScale = 0;
			Cursor.lockState = CursorLockMode.Confined;
			Cursor.visible = true;
		}
	}

	public static float GetHealthToMaxHealthRatio()
	{
		return Health/ (float)MaxHealth;
	}

	public static int GetPlayerHealth()
	{
		return Health;
	}

	public static int GetMaxPlayerHealth()
	{
		return MaxHealth;
	}

	void OnGUI()
	{
		if (!MenuManager.isAMenuEnabled)
		{
			//GUI.Box(new Rect(20, 20, Screen.width / 12, Screen.height / 13), "Health: " + Health);
			if (isDead)
			{
				GUI.Box(new Rect(Screen.width / 1.95f, Screen.height / 1.8f, Screen.width / 7f, Screen.height / 9f),
					"You have died, would you like to try again?", style);
				if (GUI.Button(new Rect(Screen.width / 1.87f, Screen.height / 1.6f, Screen.width / 9f, Screen.height / 35f), "Yes",
					ButtonStyle))
				{
					Time.timeScale = 1;
					SceneManager.LoadScene("Tutorial");					
				}
			}
		}
	}
}
