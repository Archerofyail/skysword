﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

/// <summary>
/// The common functionality and data for the player
/// </summary>
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(PlayerFlyingMovement))]
[RequireComponent(typeof(PlayerGroundMovement))]
[RequireComponent(typeof(PlayerDamageTracker))]
public class CommonPlayerData : MonoBehaviour
{

	public float cameraRotationSpeed = 100f;

	public int NumOfEnemiesNearBy
	{
		get { return Physics.OverlapSphere(transform.position, enemyCheckRadius, 1 << 9).Length; }
	}
	public bool pressedShiftInGroundScript;
	public bool isFlyingScriptEnabled;

	#region Component And Child Fields

	private PlayerFlyingMovement flyingScript;
	private PlayerGroundMovement groundScript;
	public GameObject playerModel;
	public GameObject swordModel;
	private GameObject defaultCameraPosObject;
	public Camera mainCamera;
	public ContextText ConText;
	public static Inventory inventory;
	public static List<Quest> quests;
	public static RemotePurchaseTracker remotePurchaseTracker;
	private WeaponContainer Weapon;
	private ArmourContainer Armour;
	#endregion
	#region Camera
	private bool didCameraReset;
	private bool isCameraReseting;
	#endregion


	private float enemyCheckRadius = 5f;
	private float timeNotMovedPlayer;
	private float timeNotMovedMax = 2f;
	public float movementForce = 1f;
	public float maxVelocity = 5f;

	public bool isResettingPlayerRot { get; private set; }
	public bool isMakingPlayerFaceCam { get; private set; }

	private bool isResettingCamera;

	void Start()
	{
		remotePurchaseTracker = GetComponent<RemotePurchaseTracker>();
		defaultCameraPosObject = GameObject.Find("Player/DefaultCameraPosObject");
		flyingScript = GetComponent<PlayerFlyingMovement>();
		groundScript = GetComponent<PlayerGroundMovement>();
		mainCamera = transform.Find("PlayerCamera").GetComponent<Camera>();
		StartCoroutine("MovePlayer");
		Weapon = transform.Find("WeaponContainer").GetComponent<WeaponContainer>();
		Armour = transform.Find("ArmourContainer").GetComponent<ArmourContainer>();
		ConText = mainCamera.GetComponent<ContextText>();
		inventory = new Inventory(transform.Find("ArmourContainer").GetComponent<ArmourContainer>(),
		transform.Find("WeaponContainer").GetComponent<WeaponContainer>());
		quests = new List<Quest>();
		SetData();
	}

	void OnEnable()
	{
		StartCoroutine("MovePlayer");
	}

	void OnDisable()
	{
		StopCoroutine("MovePlayer");
	}

	void Update()
	{

		if (Input.GetAxis(InputNames.Strafe) != 0 || Input.GetAxis(InputNames.ForwardBackward) != 0 && GetComponent<Animation>()["FlyingHover"].enabled)
		{
			GetComponent<Animation>().Stop("FlyingHover");
		}
		#region Developer Tools

		if (Input.GetKeyDown(KeyCode.B))
		{
			MenuManager.GoToMenu("GameScreen", "GameScreen");
		}

		if (Input.GetKeyDown(KeyCode.K))
		{
			SendMessage("DoDamage", Random.Range(2, 6), SendMessageOptions.DontRequireReceiver);
		}

		if (Input.GetKeyDown(KeyCode.P))
		{
			foreach (var inventoryItem in inventory.InventoryItems)
			{
				Debug.Log(inventoryItem.FormattedStats());
			}
		}

		if (Input.GetKeyDown(KeyCode.F))
		{
			print("Set shop items");
			MenuManager.SetShopMenuItems("First Shop");
		}

		if (Input.GetKeyDown(KeyCode.KeypadPlus))
		{
			quests.Add(new Quest());
		}
		#endregion
		if (flyingScript.enabled)
		{
			isFlyingScriptEnabled = true;
		}
		else
		{
			isFlyingScriptEnabled = false;
		}
		if (groundScript.isAttacking)
		{
			Weapon.isAttacking = true;
		}
		else
		{
			Weapon.isAttacking = false;
		}
		GetComponent<Rigidbody>().velocity = isFlyingScriptEnabled
			? Vector3.ClampMagnitude(GetComponent<Rigidbody>().velocity, maxVelocity*2)
			: Vector3.ClampMagnitude(GetComponent<Rigidbody>().velocity, maxVelocity);

		#region Keybinds
		

		if (Input.GetButtonDown(InputNames.PrimaryAttack))
		{
			StartCoroutine("MakePlayerFaceCamera");
			Weapon.Attack();
		}
		else
		{
			Weapon.CheckForReset();
		}

		#endregion

		if (groundScript.enabled)
		{
			if (!groundScript.isInAir)
			{
				GetComponent<Rigidbody>().drag = 3f;
			}
			else
			{
				GetComponent<Rigidbody>().drag = 0;
			}
		}
		else
		{
			if (Input.GetAxis(InputNames.ForwardBackward) == 0 && Input.GetAxis(InputNames.Strafe) == 0 && GetComponent<Rigidbody>().velocity.magnitude < 0.5f)
			{
				GetComponent<Rigidbody>().drag = Mathf.Pow(GetComponent<Rigidbody>().velocity.magnitude, 4);
			}
			else
			{
				GetComponent<Rigidbody>().drag = Mathf.Clamp(Mathf.Log(GetComponent<Rigidbody>().velocity.magnitude, 2), 0, 7);				
			}
		}
	}

	void FixedUpdate()
	{
		GetComponent<Rigidbody>().AddRelativeForce(Input.GetAxis(InputNames.Strafe) * movementForce, 0,
		Input.GetAxis(InputNames.ForwardBackward) * movementForce);
	}

	void OnCollisionEnter(Collision other)
	{
		groundScript.isInAir = false;
	}

	void OnCollisionStay(Collision other)
	{
		if (other.gameObject.tag == Tags.Ground)
		{
			AirToGround();
		}
	}

	void OnCollisionExit(Collision other)
	{
		groundScript.isInAir = true;
	}

	public IEnumerator MovePlayer()
	{
		while (true)
		{

			//Rotates the camera around the player in its x and y axes
			mainCamera.transform.RotateAround(transform.position, mainCamera.transform.up,
	Input.GetAxis(InputNames.LookHorizontal) * cameraRotationSpeed);
			mainCamera.transform.RotateAround(transform.position, mainCamera.transform.right,
	-Input.GetAxis(InputNames.LookVertical) * cameraRotationSpeed);
			mainCamera.transform.LookAt(transform);

			if (Input.GetAxis(InputNames.Strafe) == 0 && Input.GetAxis(InputNames.ForwardBackward) == 0 && isMakingPlayerFaceCam)
			{
				StopCoroutine("MakePlayerFaceCamera");
				isMakingPlayerFaceCam = false;
			}
			else if (Input.GetAxis(InputNames.Strafe) != 0 || Input.GetAxis(InputNames.ForwardBackward) != 0)
			{
				if (!isMakingPlayerFaceCam)
				{
					StartCoroutine("MakePlayerFaceCamera");
				}
			}
			if (Input.GetAxis(InputNames.Strafe) != 0 || Input.GetAxis(InputNames.ForwardBackward) != 0 || Input.GetAxis(InputNames.LookHorizontal) != 0 ||
						Input.GetAxis(InputNames.LookVertical) != 0)
			{
				StopCoroutine("ResetCameraRotation");
				timeNotMovedPlayer = 0;
			}
			else
			{
				timeNotMovedPlayer += Time.deltaTime;
				if (timeNotMovedPlayer > timeNotMovedMax)
				{
					timeNotMovedPlayer = 0;
					//StartCoroutine("ResetPlayerRotation");
					if (!isResettingCamera)
					{
						StartCoroutine("ResetCameraRotation");
					}
				}
				if (!isResettingPlayerRot)
				{
					StartCoroutine("ResetPlayerRotation");
				}
			}
			yield return null;
		}
	}

	private IEnumerator MakePlayerFaceCamera()
	{
		//Makes sure that this correctly rotates the player depending on whether they're flying or not
		while (((transform.rotation != mainCamera.transform.rotation) && (isFlyingScriptEnabled || groundScript.isInAir)) ||
	   ((transform.rotation.y != mainCamera.transform.rotation.y) && !isFlyingScriptEnabled))
		{
			isMakingPlayerFaceCam = true;
			//unparent the camera, rotate yourself so you're aligned with where the camera is
			mainCamera.transform.parent = null;
			var rotationResult = Quaternion.RotateTowards(transform.rotation,
				isFlyingScriptEnabled
					//If you're in the air in any way, use the camera's full rotation
					? mainCamera.transform.rotation
					: Quaternion.Euler(transform.rotation.eulerAngles.x, mainCamera.transform.rotation.eulerAngles.y,
						transform.rotation.eulerAngles.z), 25);
			transform.rotation = rotationResult;
			//Then make the parent yourself to the camera
			mainCamera.transform.parent = transform;
			yield return null;
		}
		isMakingPlayerFaceCam = false;
	}

	IEnumerator ResetPlayerRotation()
	{
		while (transform.rotation != Quaternion.Euler(0, transform.eulerAngles.y, 0))
		{
			mainCamera.transform.parent = null;
			isResettingPlayerRot = true;
			var resultingRot = Quaternion.RotateTowards(transform.rotation,
			Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0), 1.5f);
			transform.rotation = resultingRot;
			yield return null;
		}
		mainCamera.transform.parent = transform;
		timeNotMovedPlayer = 0;
		isResettingPlayerRot = false;
	}

	IEnumerator ResetCameraRotation()
	{
		while (mainCamera.transform.position != defaultCameraPosObject.transform.position)
		{
			isResettingCamera = true;
			var resultingPos = Vector3.Slerp(mainCamera.transform.position,
	defaultCameraPosObject.transform.position, 0.034f);
			mainCamera.transform.position = resultingPos;
			mainCamera.transform.LookAt(transform);
			yield return null;
		}
		isResettingCamera = false;
	}

	public void GroundToAir()
	{
		if (enabled)
		{
			StopCoroutine("ResetPlayerRotation");
			isFlyingScriptEnabled = true;
			groundScript.enabled = false;
			flyingScript.enabled = true;
		}
	}

	public void AirToGround()
	{
		if (enabled)
		{
			StartCoroutine("ResetPlayerRotation");
			isFlyingScriptEnabled = false;
			flyingScript.enabled = false;
			groundScript.enabled = true;
		}
	}

	/// <summary>
	/// Sets the player's data from the loaded game
	/// </summary>
	void SetData()
	{
		var saveGame = GameDataManager.GetLoadedGame();
		if (saveGame != null)
		{
			transform.position = saveGame.playerPos;
			transform.rotation = Quaternion.Euler(saveGame.playerRot);
			isFlyingScriptEnabled = Convert.ToBoolean(saveGame.isInAir);
			if (isFlyingScriptEnabled)
			{
				GroundToAir();
			}
			else
			{
				AirToGround();
			}
			if (saveGame.Items.Count > 0)
			{
				inventory.AddRange(saveGame.Items.ToArray());
			}
			else
			{
				inventory.InventoryItems = new List<Item>();
			}
			if (saveGame.Equipped.Count > 0)
			{
				SetEquipped(saveGame.Equipped);
			}
		}
		else
		{
			inventory.InventoryItems = new List<Item>();
		}

	}

	/// <summary>
	/// Gets the currently equipped items from the Armor and weapon containers
	/// </summary>
	/// <returns></returns>
	public List<Item> GetEquipped()
	{
		List<Item> equipped = new List<Item>(6);
		int i = 0;
		foreach (var armourPiece in Armour.equippedArmour.Keys)
		{
			equipped.Add(Armour.equippedArmour[armourPiece]);
			i++;
		}
		if (Weapon.EquippedWeapon != null)
		{
			equipped.Add(Weapon.EquippedWeapon);
		}
		return equipped;
	}

	/// <summary>
	/// Sets all the equipped items. Used for loading from a saved game
	/// </summary>
	/// <param name="equippedItems"></param>
	void SetEquipped(List<Item> equippedItems)
	{
		foreach (var equippedItem in equippedItems)
		{
			inventory.Equip(equippedItem);
		}
	}
}
