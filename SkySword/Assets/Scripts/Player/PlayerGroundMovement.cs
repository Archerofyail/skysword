﻿using System;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using UnityEngine;
using System.Collections;

/// <summary>
/// Controls the ground-specific movement of the player
/// </summary>
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(PlayerFlyingMovement))]
public class PlayerGroundMovement : MonoBehaviour
{
	[SerializeField]
	private float movementForce = 1000f;
	[SerializeField]
	private float inAirMovementForce = 500;
	[SerializeField]
	public float jumpForce = 600f;
	[SerializeField]
	public float timeBeforeAttackReset = 0.7f;
	#region Components and Children
	[SerializeField]
	private CommonPlayerData commonItems;
	#endregion
	public bool isInAir;
	public bool isFirstEnable = true;

	private float timeDiveAttacking;
	[SerializeField]
	private float timeDiveAttackingCap = 2.5f;
	private bool isDiveAttacking;
	private bool isDiveCooling;
	public bool isAttacking { get; private set; }
	
	void Start()
	{
		commonItems = GetComponent<CommonPlayerData>();
		
	}

	private void OnEnable()
	{
		if (!isFirstEnable)
		{
			GetComponent<Rigidbody>().useGravity = true;
			commonItems.playerModel.transform.rotation = transform.rotation;

			commonItems.pressedShiftInGroundScript = false;
			commonItems.movementForce = movementForce;
		}

		isFirstEnable = false;
		isInAir = true;
	}

	void OnDisable()
	{
		isDiveAttacking = false;
		isInAir = false;
	}

	void Update()
	{
		#region Movement

		if (Physics.Raycast(transform.position, Vector3.down, 5) && GetComponent<Rigidbody>().velocity.y > (commonItems.maxVelocity / 2))
		{
			GetComponent<Rigidbody>().AddForce(Vector3.up * 30);
		}

		if (!Physics.Raycast(transform.position, Vector3.down, 1))
		{
			commonItems.movementForce = inAirMovementForce;
		}
		if (Input.GetKeyDown(KeyCode.Space) && !isInAir)
		{

			GetComponent<Rigidbody>().AddRelativeForce(0, jumpForce, 0);
		}
		#endregion
		#region Combat


		if (Input.GetMouseButtonDown(1) && isInAir &&
			Physics.Raycast(transform.position, commonItems.mainCamera.transform.forward, Mathf.Infinity, 1 << 8) &&
			!isDiveCooling)
		{
			isDiveAttacking = true;
			commonItems.StartCoroutine("MakePlayerFaceCamera");
			commonItems.StopCoroutine("MovePlayer");

		}
		if (isDiveAttacking)
		{
			isAttacking = true;
			if (!commonItems.isMakingPlayerFaceCam)
			{
				GetComponent<Rigidbody>().AddRelativeForce(0, 0, 300, ForceMode.Acceleration);
			}
			timeDiveAttacking += Time.deltaTime;
			if (timeDiveAttacking > timeDiveAttackingCap)
			{
				timeDiveAttacking = 0;
				isDiveAttacking = false;
				isAttacking = false;
				commonItems.StartCoroutine("ResetPlayerRotation");
				commonItems.StartCoroutine("MovePlayer");
			}

		}

		#endregion

		#region Miscellaneous Key Inputs

		if (Input.GetButtonDown(InputNames.Boost) && isInAir)
		{
			commonItems.pressedShiftInGroundScript = true;
			commonItems.GroundToAir();
		}
		#endregion
	}

	IEnumerator DiveCooldown()
	{
		float diveCDTime = 0;
		float diveCDMax = 1f;
		while (diveCDTime < diveCDMax)
		{
			isDiveCooling = true;
			yield return null;
		}
		isDiveCooling = false;
	}

	void OnCollisionEnter(Collision other)
	{
		//If you've collided with something tagged ground, you're obviously not in the air anymore
		if (other.gameObject.tag == Tags.Ground)
		{
			commonItems.movementForce = movementForce;
			isInAir = false;
			if (isDiveAttacking)
			{
				commonItems.StartCoroutine("ResetPlayerRotation");
				commonItems.StartCoroutine("MovePlayer");
			}

			timeDiveAttacking = 0;
			isDiveAttacking = false;

		}
	}
}
