﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

/// <summary>
/// Manages the player's inventory
/// </summary>
[Serializable]
public class Inventory
{
	public List<Item> InventoryItems;
	public ArmourContainer ArmourManager { get; private set; }
	public WeaponContainer WeaponManager { get; private set; }

	public Inventory(ArmourContainer armourContainer, WeaponContainer weaponContainer)
	{
		ArmourManager = armourContainer;
		WeaponManager = weaponContainer;
		InventoryItems = new List<Item>();
	}

	public Inventory()
	{
		
	}
	
	public void Equip (Item item)
	{
		if (item != null)
		{
			InventoryItems.Remove(item);
			
			if (item.GetType() == typeof (Armour))
			{
				ArmourManager.EquipArmor((Armour) item);

			}
			else if (item.GetType() == typeof (Weapon))
			{
				WeaponManager.EquipWeapon((Weapon) item);
			}
			else
			{
				throw new NotImplementedException();
			}
		}
	}

	public void Add(Item itemToAdd)
	{
		
		InventoryItems.Add(itemToAdd);
	}

	public void AddRange(Item[] itemsToAdd)
	{
		InventoryItems.AddRange(itemsToAdd);
	}
}
