﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

/// <summary>
/// Controls menu openings and switching from flight to ground movement
/// </summary>
public class PlayerControlHandler : MonoBehaviour
{

	public static MonoBehaviour[] playerControlScripts;
	public static bool AreScriptsEnabled { get; private set; }
	private static bool[] lastEnabledScripts;

	public ContextText ConText;

	void Start()
	{
		playerControlScripts = new MonoBehaviour[]
		{GetComponent<CommonPlayerData>(), GetComponent<PlayerGroundMovement>(), GetComponent<PlayerFlyingMovement>()};
		lastEnabledScripts = new[] { true, true, false };
		ConText = transform.Find("PlayerCamera").GetComponent<ContextText>();
	}

	private void Update()
	{
		if (Time.timeScale > 0.02f)
		{

			if (Input.GetButtonDown(InputNames.Interact))
			{
				if (ConText.targetTag == Tags.Pickup && ConText.hitItem)
				{
					CommonPlayerData.inventory.Add(ConText.rayInfo.transform.GetComponent<Pickup>().Item);
					Destroy(ConText.rayInfo.transform.gameObject);
				}
				else if (ConText.targetTag == Tags.ShopKeep)
				{
					MenuManager.GoToMenu(MenuClassNames.ShopMenu, MenuManager.GetEnabledMenuName());
					MenuManager.SetShopMenuItems(ConText.rayInfo.transform.GetComponent<ShopKeeper>().ShopName);
				}
				else if (ConText.targetTag == Tags.QuestGiver)
				{
					MenuManager.GoToMenu(MenuClassNames.ViewQuestMenu, MenuManager.GetEnabledMenuName());
					MenuManager.SetQuests(ConText.rayInfo.transform.GetComponent<QuestGiver>().quests);
				}
			}

			if (Input.GetButtonDown(InputNames.OpenInventory))
			{
				MenuManager.GoToMenu(MenuClassNames.InventoryMenu, MenuManager.GetEnabledMenuName());
				MenuManager.SetInventoryMenuItems(CommonPlayerData.inventory);
			}

			if (Input.GetButtonDown(InputNames.OpenQuestLog))
			{
				MenuManager.GoToMenu(MenuClassNames.QuestLogMenu, MenuManager.GetEnabledMenuName());
				MenuManager.SetQuestLogQuests(CommonPlayerData.quests);
			}

			if (Input.GetButtonDown(InputNames.OpenMap))
			{
				MenuManager.GoToMenu(MenuClassNames.MapMenu, MenuManager.GetEnabledMenuName());
			}

			if (Input.GetButtonDown(InputNames.OpenSkillTree))
			{
				MenuManager.GoToMenu(MenuClassNames.SkillTreeMenu, MenuManager.GetEnabledMenuName());
			}
			if (Input.GetButtonDown(InputNames.OpenRemotePurchase))
			{
				MenuManager.GoToMenu(MenuClassNames.RemotePurchaseMenu, MenuManager.GetEnabledMenuName());
			}
		}
	}

	public static void EnablePlayerControl()
	{
		if (playerControlScripts != null)
		{
			int index = 0;
			foreach (var script in playerControlScripts)
			{
				script.enabled = lastEnabledScripts[index];
				index++;
			}

			AreScriptsEnabled = true;
		}
	}

	public static void DisablePlayerControl()
	{
		if (playerControlScripts != null)
		{
			int index = 0;
			foreach (var script in playerControlScripts)
			{
				lastEnabledScripts[index] = script.enabled;
				script.enabled = false;
				index++;
			}
			AreScriptsEnabled = false;
		}
	}

}
