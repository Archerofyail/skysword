﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Transporter : MonoBehaviour
{
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == Tags.Player)
		{
			SceneManager.LoadScene("GameScene");
		}
	}
}
