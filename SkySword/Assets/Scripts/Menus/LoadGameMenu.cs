﻿using UnityEngine;

public class LoadGameMenu : Menu
{
	public GUISkin guiSkin;
	public bool hasSelectedDelete;

	private Vector2 scrollPos;
	private int selectedGameIndex = -1;
	public Rect loadGameScrollViewScreenRect = new Rect(0.1f, 0.125f, 0.8f, 0.77f);

	public Rect LoadGameScrollViewScreenRect
	{
		get
		{
			return new Rect(Screen.width * loadGameScrollViewScreenRect.x, Screen.height * loadGameScrollViewScreenRect.y,
				Screen.width * loadGameScrollViewScreenRect.width, Screen.height * loadGameScrollViewScreenRect.height);
		}
	}

	void OnGUI()
	{

		GUI.skin = guiSkin;
		GUI.enabled = !hasSelectedDelete;
		GUILayout.BeginArea(LoadGameScrollViewScreenRect);
		scrollPos = GUILayout.BeginScrollView(scrollPos);
		int counter = 0;
		foreach (var saveGame in GameDataManager.GetGames())
		{

			if (GUILayout.Button(
				"Level: " + saveGame.sceneName +
				"\nPlayerPos: " + saveGame.playerPos +
				"\nPlayerRot:" + saveGame.playerRot,
				counter == selectedGameIndex ? guiSkin.GetStyle("selectedbutton") : guiSkin.GetStyle("Button"),
				GUILayout.MinHeight(Screen.height * 0.2f), GUILayout.MinWidth(LoadGameScrollViewScreenRect.width)))
			{
				selectedGameIndex = counter;

			}
			counter++;
			GUILayout.Space(Screen.height * 0.01f);
		}

		GUILayout.EndScrollView();
		GUILayout.EndArea();
		if (GUI.Button(MenuManager.MenuBackButtonRect(), "Back"))
		{
			//print("sender is " + Sender);
			MenuManager.GoToMenu(MenuClassNames.MainMenu, MenuClassNames.LoadGameMenu);
		}
		GUI.enabled = selectedGameIndex > -1 && !hasSelectedDelete;

		if (GUI.Button(new Rect(Screen.width * 0.85f, Screen.height * 0.92f, Screen.width * 0.14f, Screen.height * 0.05f), "Load"))
		{
			GameDataManager.LoadGame(selectedGameIndex);
			GetComponent<PickupPlacer>().enabled = true;
			enabled = false;
		}
		if (GUI.Button(new Rect(Screen.width * 0.7f, Screen.height * 0.92f, Screen.width * 0.14f, Screen.height * 0.05f), "Delete"))
		{
			hasSelectedDelete = true;
		}

		GUI.enabled = true;
		if (hasSelectedDelete)
		{

			GUI.Box(new Rect(Screen.width * 0.42f, Screen.height * 0.45f, Screen.width * 0.21f, Screen.height * 0.15f),
				"Are you sure you want to delete this save?");
			if (GUI.Button(new Rect(Screen.width * 0.435f, Screen.height * 0.55f, Screen.width * 0.08f, Screen.height * 0.03f), "Yes"))
			{
				GameDataManager.DeleteGame(selectedGameIndex);
			}
			if (GUI.Button(new Rect(Screen.width * 0.535f, Screen.height * 0.55f, Screen.width * 0.08f, Screen.height * 0.03f), "No"))
			{
				hasSelectedDelete = false;
			}

		}
	}
}


