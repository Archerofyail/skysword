﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class QuestLogMenu : Menu
{
	public GUISkin guiSkin;
	private List<Quest> quests;
	private int selectedQuestIndex = -1;
	private Vector2 questScrollViewPos;

	void OnGUI()
	{
		GUI.skin = guiSkin;
		GUILayout.BeginArea(new Rect(Screen.width * 0.05f, Screen.height * 0.05f, Screen.width * 0.22f, Screen.height * 0.85f));
		questScrollViewPos = GUILayout.BeginScrollView(questScrollViewPos, false, true, GUIStyle.none,
			guiSkin.GetStyle("verticalscrollbar"), GUILayout.MinHeight(Screen.height*0.85f));
		if (quests != null)
		{
			if (quests.Count > 0)
			{
				int count = 0;
				foreach (var quest in quests)
				{
					if (GUILayout.Button(quest.questName,
						count == selectedQuestIndex ? guiSkin.GetStyle("selectedbutton") : guiSkin.GetStyle("Button"),
						GUILayout.MinWidth(Screen.width * 0.2f)))
					{
						selectedQuestIndex = count;
					}
					count++;
					GUILayout.Space(Screen.height * 0.01f);
				}
			}
			else
			{
				GUILayout.Label("No Active Quests", GUILayout.MinWidth(Screen.width * 0.2f));
			}
		}
		else
		{
			GUILayout.Label("No Active Quests", GUILayout.MinWidth(Screen.width * 0.2f));
		}
		GUILayout.EndScrollView();
		GUILayout.EndArea();
		if (selectedQuestIndex > -1 && selectedQuestIndex <= quests.Count)
		{
			GUILayout.BeginArea(new Rect(Screen.width * 0.3f, Screen.height * 0.05f, Screen.width * 0.6f, Screen.height * 0.85f));
			GUILayout.Box(quests[selectedQuestIndex].questName, GUILayout.MinHeight(50));
			GUILayout.Box(quests[selectedQuestIndex].questDescription, GUILayout.MinHeight(150));
			GUILayout.BeginVertical(guiSkin.GetStyle("Box"), GUILayout.MinHeight(100));
			//GUILayout.Box(quests[selectedQuestIndex].objectiveDescription, GUILayout.MinHeight(100));
			//GUI.enabled = false;
			foreach (var questObjective in quests[selectedQuestIndex].questObjectives)
			{
				GUILayout.Toggle(questObjective.isCompleted, questObjective.ObjectiveString());
			}
			//GUI.enabled = true;
			GUILayout.EndVertical();
			GUILayout.EndArea();
		}
		else
		{
			GUILayout.BeginArea(new Rect(Screen.width * 0.3f, Screen.height * 0.05f, Screen.width * 0.6f, Screen.height * 0.85f));
			GUILayout.Box("", GUILayout.MinHeight(50));
			GUILayout.Box("", GUILayout.MinHeight(150));
			GUILayout.Box("", GUILayout.MinHeight(100));
			GUILayout.EndArea();
		}
	}

	/// <summary>
	/// Sets the quests shown by the quest log
	/// </summary>
	/// <param name="questsToSet">The quests to show</param>
	public void SetQuests(List<Quest> questsToSet)
	{
		quests = questsToSet;
	}
}
