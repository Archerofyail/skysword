﻿using UnityEngine;
using System.Collections;

public class OptionsMenu : Menu
{
	public GUISkin guiSkin;
	private Vector2 scrollPos;
	public Rect optionScrollViewScreenRect = new Rect(0.1f, 0.125f, 0.8f, 0.77f);
	private float sliderVal = 2;
	public Rect OptionScrollViewScreenRect
	{
		get
		{
			return new Rect(Screen.width * optionScrollViewScreenRect.x, Screen.height * optionScrollViewScreenRect.y,
				Screen.width * optionScrollViewScreenRect.width, Screen.height * optionScrollViewScreenRect.height);
		}
	}

	void OnGUI()
	{
		GUI.skin = guiSkin;
		GUILayout.BeginArea(OptionScrollViewScreenRect);
		scrollPos = GUILayout.BeginScrollView(
			scrollPos);
		GUILayout.Box("Test box");
		GUILayout.Label(sliderVal + "", guiSkin.GetStyle("centeredlabel"));
		sliderVal = GUILayout.HorizontalSlider(sliderVal, -5, 5);
		GUILayout.EndScrollView();
		GUILayout.EndArea();
		if (GUI.Button(MenuManager.MenuBackButtonRect(), "Back"))
		{
			if (GetComponent<MainMenu>())
			{
				MenuManager.GoToMenu(MenuClassNames.MainMenu, MenuClassNames.OptionsMenu);
			}
			else
			{
				MenuManager.GoToMenu(MenuClassNames.PauseMenu, MenuClassNames.OptionsMenu);
				
			}
		}
	}
}
