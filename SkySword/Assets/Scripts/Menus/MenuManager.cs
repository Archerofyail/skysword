﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class MenuManager
{
	public static bool isAMenuEnabled { get; private set; }
	private static bool wasAMenuEnabled;
	public static GameObject menuManager;
	private static List<Menu> menus;

	public static void SetMenuManagerObject(GameObject managerObject)
	{

		menuManager = managerObject;
		menus = new List<Menu>();
		foreach (var field in Type.GetType("MenuClassNames").GetFields())
		{
			var menu = (Menu)menuManager.GetComponent(field.GetRawConstantValue().ToString());
			if (menu)
			{
				menus.Add(menu);
			}
		}
	}

	public static void Update()
	{
		//If you've pressed escape
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Menu enabledmenu;
			IsAMenuEnabled(out enabledmenu);

			string dest;
			string sender;
			//And a menu is enabled
			if (enabledmenu)
			{
				//and the menu has a sender
				if (enabledmenu.Sender == MenuClassNames.PauseMenu || enabledmenu.Sender == "Sender")
				{
					//Go back to the sender menu
					dest = enabledmenu.Sender;
				}
				else
				{
					//Otherwise go back to the gamescreen
					dest = "GameScreen";
				}

				sender = enabledmenu.GetType().ToString();
			}
			else
			{
				//If no menu is enabled, go to the pause menu
				dest = MenuClassNames.PauseMenu;
				sender = "GameScreen";
			}
			if (sender != MenuClassNames.MainMenu)
			{
				GoToMenu(dest, sender);
			}
		}
		isAMenuEnabled = IsAMenuEnabled();
		if (isAMenuEnabled != wasAMenuEnabled)
		{
			if (isAMenuEnabled)
			{
				isAMenuEnabled = true;
				PlayerControlHandler.DisablePlayerControl();
				Cursor.visible = true;
				Cursor.lockState = CursorLockMode.None;
				Time.timeScale = 0.05f;
			}
			else
			{
				isAMenuEnabled = false;
				PlayerControlHandler.EnablePlayerControl();
				Cursor.visible = false;
				Cursor.lockState = CursorLockMode.Locked;
				Time.timeScale = 1;
			}
		}
		wasAMenuEnabled = isAMenuEnabled;
	}

	public static void GoToMenu(string dest, string sender)
	{

		var senderMenu = (Menu)menuManager.GetComponent(sender);
		string destName = dest;
		if (senderMenu)
		{
			senderMenu.enabled = false;
		}
		var destMenu = (Menu)menuManager.GetComponent(destName);
		if (destMenu)
		{
			//if (destMenu.enabled)
			//{
			//	destMenu.enabled = false;
			//	return;
			//}
			if (sender != dest)
			{
				destMenu.Sender = sender;
				destMenu.enabled = true;

				//If you're navigating to a menu from the pause menu or main menu, change the destination's sender to the that menu
				if (sender == MenuClassNames.PauseMenu || sender == MenuClassNames.MainMenu)
				{
					destMenu.Sender = senderMenu.GetType().ToString();
				}
			}
		}
	}

	public static string GetEnabledMenuName()
	{
		string name = "";
		Menu enabledMenu;
		if (IsAMenuEnabled(out enabledMenu))
		{
			name = enabledMenu.GetType().ToString();
		}
		else
		{
			name = "GameScreen";
		}
		return name;
	}

	public static bool IsAMenuEnabled(out Menu enabledMenu)
	{
		foreach (var menu in menus.Where(menu => menu.enabled))
		{
			enabledMenu = menu;
			return true;
		}
		enabledMenu = null;
		return false;
	}

	public static bool IsAMenuEnabled()
	{
		return menus.Any(menu => menu.enabled);
	}

	public static void SetMenuEnabled(bool isEnabled)
	{
		isAMenuEnabled = isEnabled;
	}

	public static void SetInventoryMenuItems(Inventory inventory)
	{
		menuManager.GetComponent<InventoryMenu>().GetData(inventory);
	}

	public static void SetShopMenuItems(string shopName)
	{
		menuManager.GetComponent<ShopMenu>().GetData(shopName);
		//print("Called set shop items");
	}

	public static void SetQuests(IEnumerable<Quest> quests)
	{
		var questMenu = menuManager.GetComponent<ViewQuestMenu>();
		if (questMenu)
		{
			questMenu.quests = new List<Quest>();
			questMenu.quests.AddRange(quests);
		}
	}

	public static void SetQuestLogQuests(List<Quest> questsToSet)
	{
		menuManager.GetComponent<QuestLogMenu>().SetQuests(questsToSet);
	}

	public static void Quit()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
				Application.Quit();
#endif
	}

	public static Rect MenuBackButtonRect()
	{
		return new Rect(Screen.width * 0.025f, Screen.height * 0.95f, Screen.width * 0.07f, Screen.height * 0.03f);
	}

}