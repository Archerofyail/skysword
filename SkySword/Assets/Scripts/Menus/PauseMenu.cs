﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : Menu
{
	public GUISkin guiSkin;
	private bool pressedQuit;

	public float minButtonHeight = 0.03f;
	private float MinButtonHeight
	{
		get { return minButtonHeight * Screen.height; }
	}

	public float spaceBetweenButtons = 0.02f;
	private float SpaceBetweenButtons
	{
		get { return spaceBetweenButtons * Screen.height; }
	}

	void OnEnable()
	{
		Time.timeScale = 0;
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.Confined;
	}

	void OnGUI()
	{
		GUI.skin = guiSkin;
		GUI.enabled = !pressedQuit;
		GUILayout.BeginArea(new Rect(Screen.width * 0.45f, Screen.height * 0.32f, Screen.width * 0.14f, Screen.height * 0.4f));
		if (GUILayout.Button("Resume Game", GUILayout.ExpandWidth(true)))
		{
			Time.timeScale = 1;
			enabled = false;
		}
		GUILayout.Space(SpaceBetweenButtons);
		if (GUILayout.Button("Save Game", GUILayout.ExpandWidth(true)))
		{
			GameDataManager.SaveGame();
		}
		GUILayout.Space(SpaceBetweenButtons);
		if (GUILayout.Button("Load Game", GUILayout.ExpandWidth(true)))
		{
			MenuManager.GoToMenu(MenuClassNames.LoadGameMenu, MenuClassNames.PauseMenu);
		}
		GUILayout.Space(SpaceBetweenButtons);
		if (GUILayout.Button("Options", GUILayout.ExpandWidth(true)))
		{
			MenuManager.GoToMenu(MenuClassNames.OptionsMenu, MenuClassNames.PauseMenu);
		}
		GUILayout.Space(SpaceBetweenButtons);
		if (GUILayout.Button("Exit", GUILayout.ExpandWidth(true)))
		{
			pressedQuit = true;
		}
		GUILayout.EndArea();
		GUI.enabled = true;
		if (pressedQuit)
		{
			GUI.Box(new Rect(Screen.width / 2.2f, Screen.height / 3, Screen.width / 6, Screen.height / 7f),
				"Progress not saved will be lost,"
				+ "Do you really want to quit?", guiSkin.GetStyle("Box"));
			if (GUI.Button(new Rect(Screen.width / 2.1f, Screen.height / 2.3f, Screen.width / 19, Screen.height / 35), "Yes"))
			{
				MenuManager.GoToMenu(MenuClassNames.MainMenu, MenuClassNames.PauseMenu);
				SceneManager.LoadScene("MainMenu");

			}
			if (GUI.Button(new Rect(Screen.width / 1.82f, Screen.height / 2.3f, Screen.width / 19, Screen.height / 35), "No"))
			{
				pressedQuit = false;
			}
		}
	}
}
