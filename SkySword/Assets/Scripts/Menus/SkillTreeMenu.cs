﻿using System.IO;
using System.Linq;
using System.Xml;
using UnityEngine;
using System.Collections;

public class SkillTreeMenu : Menu
{
	public GUISkin skin;
	public Rect screenRect = new Rect(0.15f, 0.1f, 0.85f, 0.8f);
	public float skillButtonWidth = 0.05f;

	private float SkillButtonWidth
	{
		get { return skillButtonWidth * Screen.width; }

	}

	void OnEnable()
	{
		Debug.Log("Number of skill trees: " + SkillManager.SkillTrees.Count);
		if (SkillManager.SkillTrees.Count == 0)
		{
			var skillReaderSettings = new XmlReaderSettings { CloseInput = true };
			var skillReader =
				XmlReader.Create(
					new FileStream(Directory.GetCurrentDirectory() + "\\DefaultData\\Skills.xml", FileMode.Open, FileAccess.Read,
						FileShare.Read), skillReaderSettings);
			SkillManager.SetSkillTrees(GameDataManager.LoadAllSkills(skillReader));
			skillReader.Close();

		}
	}

	private Rect ScreenRect
	{
		get
		{
			return new Rect(Screen.width * screenRect.x, Screen.height * screenRect.y, Screen.width * screenRect.width,
				Screen.height * screenRect.height);
		}
	}
	void OnGUI()
	{
		GUI.skin = skin;
		GUILayout.BeginArea(ScreenRect);
		GUILayout.BeginHorizontal(GUILayout.MaxWidth(ScreenRect.width * 0.3333f));
		foreach (var skillTree in SkillManager.SkillTrees)
		{
			
			GUILayout.BeginVertical(skin.GetStyle("Box"));
			GUILayout.Label(skillTree.treeName);
			foreach (var skillTier in skillTree.skills)
			{
				GUILayout.BeginHorizontal();
				foreach (var skill in skillTier)
				{
					GUILayout.BeginVertical();
					
					bool hasUnlockedRank = false;
					int i = 0;
					foreach (var point in skillTree.pointsInTiers)
					{
						if (i + 1 == skill.tier && point >= 5)
						{
							hasUnlockedRank = true;
							break;
						}
						i++;
					}
					GUI.enabled =
						(skillTree.GetSkillsByName(skill.dependentSkills.ToArray()).All(dependentSkill => dependentSkill.rank > 0) &&
						 hasUnlockedRank) || skill.tier == 0;
					if (skill.skillTex)
					{
						if (GUILayout.Button(new GUIContent(skill.skillTex, "<b>" +skill.skillName + "</b>" + "\n" + skill.skillDescription),
							GUILayout.MinWidth(SkillButtonWidth), GUILayout.MinHeight(SkillButtonWidth)))
						{
							skill.rank++;
							skillTree.SumPointsInTiers();
						}
					}
					else
					{
						if (GUILayout.Button(new GUIContent(skill.skillName, "<b>" + skill.skillName + "</b>" + "\n" + skill.skillDescription), GUILayout.MinWidth(SkillButtonWidth), GUILayout.MinHeight(SkillButtonWidth)))
						{
							skill.rank++;
							skillTree.SumPointsInTiers();
						}
					}
					GUI.enabled = true;
					GUILayout.Label(skill.skillName, skin.GetStyle("SkillNameLabel"), GUILayout.MaxWidth(SkillButtonWidth));
					GUILayout.EndVertical();

				}
				GUILayout.EndHorizontal();

			}
			GUILayout.EndVertical();
		}
		GUILayout.EndHorizontal();
		GUILayout.EndArea();
		if (GUI.tooltip != "")
		{
			var mousePos = Event.current.mousePosition;
			GUI.Box(new Rect(mousePos.x, mousePos.y + 15, Screen.width * 0.25f, Screen.height * 0.1f),
				GUI.tooltip);
		}
	}
}
