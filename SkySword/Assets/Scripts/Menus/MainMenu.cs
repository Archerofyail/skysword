﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : Menu
{
	public Texture2D TitleImage;
	public Texture2D backgroundImage;
	public GUISkin guiSkin;
	private bool pressedQuit;

	public float minButtonHeight = 0.07f;
	public float spaceBetweenButtons = 0.07f;
	public float SpaceBetweenButtons
	{
		get { return Screen.height * spaceBetweenButtons; }
	}

	public GUILayoutOption MinButtonHeight
	{
		get { return GUILayout.MinHeight(Screen.height * minButtonHeight); }
	}

	void Start()
	{
		DontDestroyOnLoad(gameObject);
		MenuManager.SetMenuManagerObject(gameObject);
	}

	void OnGUI()
	{
		GUI.skin = guiSkin;
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundImage, ScaleMode.StretchToFill);
		GUI.DrawTexture(new Rect(Screen.width / 2.5f, Screen.width / 40, Screen.width / 4, Screen.height / 4), TitleImage, ScaleMode.ScaleToFit);
		GUI.enabled = !pressedQuit;
		GUILayout.BeginArea(new Rect(Screen.width * 0.45f, Screen.height * 0.35f, Screen.width * 0.14f, Screen.height * 0.3f));
		if (GUILayout.Button("New Game", MinButtonHeight, GUILayout.ExpandWidth(true)))
		{
			enabled = false;
			SceneManager.LoadScene("Tutorial");
		}
		GUILayout.Space(SpaceBetweenButtons);
		if (GUILayout.Button("Load Game", MinButtonHeight, GUILayout.ExpandWidth(true)))
		{
			MenuManager.GoToMenu(MenuClassNames.LoadGameMenu, MenuClassNames.MainMenu);
		}
		GUILayout.Space(SpaceBetweenButtons);
		if (GUILayout.Button("Options", MinButtonHeight, GUILayout.ExpandWidth(true)))
		{
			MenuManager.GoToMenu(MenuClassNames.OptionsMenu, MenuClassNames.MainMenu);
		}
		GUILayout.Space(SpaceBetweenButtons);
		if (GUILayout.Button("Exit", MinButtonHeight, GUILayout.ExpandWidth(true)))
		{
			pressedQuit = true;
		}
		GUILayout.EndArea();
		if (pressedQuit)
		{
			MenuManager.Quit();
		}
	}
}
