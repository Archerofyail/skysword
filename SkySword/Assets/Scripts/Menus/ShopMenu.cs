﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;
using Random = UnityEngine.Random;

public class ShopMenu : Menu
{
	public string ShopName;
	public GUISkin skin;

	public Shop Inventory;
	public Item selectedItem;
	[SerializeField]
	private List<Item> displayedItems;
	private Vector2 itemsListScrollPos;
	private Vector2 categorySelectorListScrollPos;
	private Vector2 filterSelectorScrollPos;
	private List<Type> ItemTypes;
	private List<string> ItemFilters;
	private bool isCategorySelected;
	private int selectedCategory = -1;
	private bool isFilterSelected;
	private int selectedFilter = -1;
	private string selectedFilterName = "";

	public bool isRemote;

	#region GUIItemPositions

	public float topItemPositions = 0.03f;

	public float TopItemPositions
	{
		get { return Screen.height*topItemPositions; }
	}

	public Vector2 categoryAndFilterWidth = new Vector2(0.2f, 0.87f);
	public Vector2 itemListWidth = new Vector2(0.73f, 0.2f);

	public Vector2 CategoryAndFilterWidth
	{
		get { return new Vector2(Screen.height * categoryAndFilterWidth.x, Screen.height * categoryAndFilterWidth.y); }
	}

	public Vector2 ItemListWidth
	{
		get { return new Vector2(itemListWidth.x * Screen.width, itemListWidth.y * Screen.height); }
	}

	#endregion

	void Awake()
	{
		categorySelectorListScrollPos = new Vector2();
		ItemFilters = new List<string>();
	}

	void OnEnable()
	{
		isRemote = (Sender == MenuClassNames.RemotePurchaseMenu || Sender == MenuClassNames.MapMenu);
		print("Remote Purchase? " + isRemote + ". Sender is " + Sender);
	}

	void OnDisable()
	{
		GameDataManager.AddShop(ShopName, Inventory);
	}

	public void GetData(string shopName)
	{

		ItemTypes = new List<Type>();
		foreach (var type in Assembly.GetExecutingAssembly().GetTypes())
		{
			if (type.IsSubclassOf(typeof(Item)))
			{
				ItemTypes.Add(type);
			}
		}
		displayedItems = new List<Item>();
		Inventory = new Shop(LoadShopItems(shopName));
		displayedItems.AddRange(Inventory.InventoryItems);
		var shopKeeper = FindObjectOfType<ShopKeeper>();
		if (shopKeeper)
		{
			if (shopKeeper.ShopName == shopName)
			{
				Inventory.worldPos = shopKeeper.transform.position;
			}
		}
		UpdateItems();
	}

	List<Item> LoadShopItems(string shopName)
	{
		List<Item> itemsLoaded = new List<Item>();
		var loadedGame = GameDataManager.GetLoadedGame();
		if (loadedGame != null)
		{

			print((loadedGame.Shops.ContainsKey(shopName)));
			if (loadedGame.Shops.ContainsKey(shopName))
			{
				if (loadedGame.Shops.ContainsKey(shopName))
				{
					if (loadedGame.Shops[shopName].InventoryItems.Count > 0)
					{
						itemsLoaded.AddRange(loadedGame.Shops[shopName].InventoryItems.ToArray());
					}
				}
			}
			else
			{
				itemsLoaded = GetRandomInventory();
			}
		}
		else
		{
			itemsLoaded = GetRandomInventory();
		}

		return itemsLoaded;
	}

	private List<Item> GetRandomInventory()
	{
		var randItems = new List<Item>();
		var randInt = Random.Range(15, 20);
		for (int i = 0; i < randInt; i++)
		{
			var randType = Random.Range(0, ItemTypes.Count);
			var randItem = (Item)Activator.CreateInstance(ItemTypes[randType]);
			randItem.GetRandom();
			randItems.Add(randItem);
		}
		return randItems;
	}

	void UpdateItems()
	{
		if (isCategorySelected)
		{
			displayedItems = new List<Item>();
			var categorizedItems =
				Inventory.InventoryItems.Where(item => item.GetType().ToString() == ItemTypes[selectedCategory].ToString());
			List<Item> filteredItems = new List<Item>();
			if (isFilterSelected)
			{

				filteredItems.AddRange(
					Inventory.InventoryItems.Where(
						item => item.GetType() == ItemTypes[selectedCategory] && item.enumType == selectedFilterName));
				displayedItems.AddRange(filteredItems);
				return;
			}
			displayedItems.AddRange(categorizedItems);
		}
		else
		{
			displayedItems = Inventory.InventoryItems;
		}
	}

	void GetCategoryTypes()
	{
		ResetFilter();
		Array enumVals;
		try
		{
			if (ItemTypes.Count > selectedCategory)
			{
				try
				{
					enumVals = Enum.GetValues(Type.GetType(ItemTypes[selectedCategory] + "Type"));
					foreach (var value in enumVals)
					{
						print("enum val = " + value);
						ItemFilters.Add(value.ToString());
					}
				}
				catch (Exception e)
				{
					Debug.Log(e.Message);
				}
			}
			else
			{
				print("ERROR: Failed to find enum for type " + ItemTypes[selectedCategory]);
			}
		}
		catch (Exception e)
		{
			print(e.Message);
		}

	}

	void ResetFilter()
	{
		ItemFilters = new List<string>();
	}

	void OnGUI()
	{
		GUI.skin = skin;
		#region Categories
		GUILayout.BeginArea(new Rect(Screen.width * 0.03f, TopItemPositions, CategoryAndFilterWidth.x, CategoryAndFilterWidth.y));
		categorySelectorListScrollPos = GUILayout.BeginScrollView(categorySelectorListScrollPos);
		int i = 0;
		foreach (var type in ItemTypes)
		{
			if (GUILayout.Button(type.ToString(),
				i == selectedCategory ? skin.GetStyle("selectedbutton") : skin.GetStyle("Button"),
				GUILayout.MinHeight(Screen.height * 0.17f), GUILayout.ExpandWidth(true)))
			{

				if (!isCategorySelected)
				{

					isCategorySelected = true;
					selectedCategory = i;
					GetCategoryTypes();
				}
				else
				{

					if (selectedCategory != i)
					{
						isCategorySelected = true;
						selectedCategory = i;
						GetCategoryTypes();
					}
					else
					{
						isCategorySelected = false;
						selectedCategory = -1;
						ResetFilter();
					}
				}
				UpdateItems();
			}
			i++;
			GUILayout.Space(Screen.height * 0.005f);
		}
		GUILayout.EndScrollView();
		GUILayout.EndArea();
		#endregion
		#region Filter
		GUILayout.BeginArea(new Rect(Screen.width * 0.87f, TopItemPositions, CategoryAndFilterWidth.x, CategoryAndFilterWidth.y));
		filterSelectorScrollPos =
			GUILayout.BeginScrollView(filterSelectorScrollPos);
		int j = 0;
		foreach (var filterType in ItemFilters)
		{
			if (GUILayout.Button(Regex.Replace(filterType, "(?<!^)([A-Z])", " $1"),
				selectedFilter == j ? skin.GetStyle("selectedbutton") : skin.GetStyle("Button"),
				GUILayout.MinHeight(Screen.height * 0.17f), GUILayout.ExpandWidth(true)))
			{
				if (!isFilterSelected)
				{
					isFilterSelected = true;
					selectedFilter = j;
					selectedFilterName = ItemFilters[selectedFilter];
					UpdateItems();
				}
				else
				{
					if (selectedFilter != j)
					{
						isFilterSelected = true;
						selectedFilter = j;
						selectedFilterName = ItemFilters[selectedFilter];
						UpdateItems();
					}
					else
					{
						isFilterSelected = false;
						selectedFilter = -1;
					}
				}
				UpdateItems();
			}
			j++;
			GUILayout.Space(Screen.height * 0.005f);
		}
		GUILayout.EndScrollView();
		GUILayout.EndArea();
		#endregion
		#region Items
		GUILayout.BeginArea(new Rect(Screen.width * 0.15f, TopItemPositions, Screen.width * 0.71f, Screen.height * 0.2f));
		itemsListScrollPos = GUILayout.BeginScrollView(itemsListScrollPos, true, false);
		GUILayout.BeginHorizontal();
		int counter = 0;
		foreach (var item in displayedItems)
		{
			if (GUILayout.Button(item.ItemName,
				item == selectedItem ? skin.GetStyle("selectedbutton") : skin.GetStyle("Button"),
				GUILayout.MinWidth(Screen.height * 0.17f), GUILayout.MinHeight(Screen.height * 0.17f)))
			{
				selectedItem = item;
			}
			counter++;
			GUILayout.Space(Screen.width * 0.005f);
		}
		GUILayout.EndHorizontal();
		GUILayout.EndScrollView();
		GUILayout.EndArea();
		if (selectedItem != null)
		{

			GUI.Box(new Rect(Screen.width * 0.15f, Screen.height * 0.25f, Screen.width * 0.71f, Screen.height * 0.54f),
				selectedItem.ItemName + "\n" + selectedItem.FormattedStats());
		}
		if (selectedItem != null)
		{
			//GUI.enabled = selectedItem.GetType() == typeof(Armour) || selectedItem.GetType() == typeof(Weapon);
		}
		else
		{
			GUI.enabled = false;
		}
		if (GUI.Button(new Rect(Screen.width / 2.5f, Screen.height / 1.25f, Screen.width / 4, Screen.height / 20), "Buy"))
		{
			Inventory.InventoryItems.Remove(selectedItem);
			Inventory.Buy(selectedItem, isRemote);
			UpdateItems();
		}

		#endregion
	}
}
