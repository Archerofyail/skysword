﻿using System.Linq;
using UnityEngine;
using System.Collections.Generic;

public class RemotePurchaseMenu : Menu
{
	public Dictionary<string, Shop> cachedShops;
	public GUISkin skin;

	private int shopIndex = -1;
	void OnEnable()
	{
		cachedShops = GameDataManager.shops;
	}

	void OnGUI()
	{
		if (skin)
		{
			GUI.skin = skin;
		}
		GUILayout.BeginArea(new Rect(Screen.width * 0.15f, Screen.height * 0.1f, Screen.width * 0.1f, Screen.height * 0.8f), skin.GetStyle("Box"));
		int i = 0;
		foreach (var shop in cachedShops.Keys)
		{
			if (GUILayout.Button(shop, GUILayout.ExpandWidth(true)))
			{
				shopIndex = i;
			}
			i++;
		}
		GUILayout.EndArea();
		GUILayout.BeginArea(new Rect(Screen.width * 0.27f, Screen.height * 0.1f, Screen.width * 0.65f, Screen.height * 0.8f), skin.GetStyle("Box"));
		if (shopIndex > -1)
		{
			GUILayout.Box(cachedShops.Keys.ToArray()[shopIndex], GUILayout.ExpandWidth(true));
			if (GUILayout.Button("Purchase From Here", GUILayout.ExpandWidth(true)))
			{
				MenuManager.GoToMenu(MenuClassNames.ShopMenu, MenuClassNames.RemotePurchaseMenu);
			}
		}
		GUILayout.EndArea();
	}
}
