﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;
using System.Collections;

public class InventoryMenu : Menu
{
	public GUISkin skin;
	public Inventory Inventory;
	public Item selectedItem;
	private List<Item> displayedItems;
	private Vector2 itemsListScrollPos;
	private Vector2 categorySelectorListScrollPos;
	private Vector2 filterSelectorScrollPos;
	private List<Type> ItemTypes;
	private List<string> ItemFilters;
	private bool isCategorySelected;
	private int selectedCategory = -1;
	private bool isFilterSelected;
	private int selectedFilter = -1;
	private string selectedFilterName = "";
	void Start()
	{
		categorySelectorListScrollPos = new Vector2();
		ItemFilters = new List<string>();
	}

	void OnEnable()
	{
		UpdateItems();
	}

	/// <summary>
	/// Gets the list of Item types and adds the items in Inventory to displayed items
	/// </summary>
	/// <param name="inventory"></param>
	public void GetData(Inventory inventory)
	{
		Inventory = inventory;
		ItemTypes = new List<Type>();
		foreach (var type in Assembly.GetExecutingAssembly().GetTypes())
		{
			if (type.IsSubclassOf(typeof(Item)))
			{
				ItemTypes.Add(type);
			}
		}
		displayedItems = new List<Item>();
		displayedItems.AddRange(inventory.InventoryItems);
	}

	/// <summary>
	/// Updates listed items based on category and filter selections
	/// </summary>
	void UpdateItems()
	{
		if (isCategorySelected)
		{
			displayedItems = new List<Item>();
			var categorizedItems =
				Inventory.InventoryItems.Where(item => item.GetType().ToString() == ItemTypes[selectedCategory].ToString());
			List<Item> filteredItems = new List<Item>();
			if (isFilterSelected)
			{

				filteredItems.AddRange(
					Inventory.InventoryItems.Where(
						item => item.GetType() == ItemTypes[selectedCategory] && item.enumType == selectedFilterName));
				displayedItems.AddRange(filteredItems);
				return;
			}
			displayedItems.AddRange(categorizedItems);
		}
		else
		{
			displayedItems = Inventory.InventoryItems;
		}
	}

	/// <summary>
	/// Gets the enum for each ItemType and adds those to the ItemFilters
	/// </summary>
	void GetCategoryTypes()
	{
		ResetFilter();
		Array enumVals;
		try
		{
			enumVals = Enum.GetValues(Type.GetType(ItemTypes[selectedCategory] + "Type"));
			foreach (var value in enumVals)
			{
				print("enum val = " + value);
				ItemFilters.Add(value.ToString());
			}
		}
		catch (Exception e)
		{
			print(e.Message);
		}

	}

	void ResetFilter()
	{
		ItemFilters = new List<string>();
	}

	void OnGUI()
	{
		GUI.skin = skin;
		#region Categories
		GUILayout.BeginArea(new Rect(Screen.width / 35, Screen.height / 35, Screen.width / 9, Screen.height / 1.15f));
		categorySelectorListScrollPos = GUILayout.BeginScrollView(categorySelectorListScrollPos);
		int i = 0;
		foreach (var type in ItemTypes)
		{
			if (GUILayout.Button(type.ToString(),
				i == selectedCategory ? skin.GetStyle("selectedbutton") : skin.GetStyle("Button"),
				GUILayout.MinHeight(Screen.height * 0.17f), GUILayout.ExpandWidth(true)))
			{

				if (!isCategorySelected)
				{

					isCategorySelected = true;
					selectedCategory = i;
					GetCategoryTypes();
				}
				else
				{

					if (selectedCategory != i)
					{
						isCategorySelected = true;
						selectedCategory = i;
						GetCategoryTypes();
					}
					else
					{
						isCategorySelected = false;
						selectedCategory = -1;
						ResetFilter();
					}
				}
				UpdateItems();
			}
			i++;
			GUILayout.Space(Screen.height * 0.005f);
		}
		GUILayout.EndScrollView();
		GUILayout.EndArea();
		#endregion
		#region Filter
		GUILayout.BeginArea(new Rect(Screen.width / 1.15f, Screen.height / 35, Screen.width / 9, Screen.height / 1.15f));
		filterSelectorScrollPos =
			GUILayout.BeginScrollView(
				filterSelectorScrollPos);
		int j = 0;
		foreach (var filterType in ItemFilters)
		{
			if (GUILayout.Button(Regex.Replace(filterType, "(?<!^)([A-Z])", " $1"),
				selectedFilter == j ? skin.GetStyle("selectedbutton") : skin.GetStyle("Button"),
				GUILayout.MinHeight(Screen.height * 0.17f), GUILayout.ExpandWidth(true)))
			{
				if (!isFilterSelected)
				{
					isFilterSelected = true;
					selectedFilter = j;
					selectedFilterName = ItemFilters[selectedFilter];
					UpdateItems();
				}
				else
				{
					if (selectedFilter != j)
					{
						isFilterSelected = true;
						selectedFilter = j;
						selectedFilterName = ItemFilters[selectedFilter];
						UpdateItems();
					}
					else
					{
						isFilterSelected = false;
						selectedFilter = -1;
					}
				}
				UpdateItems();
			}
			j++;
			GUILayout.Space(Screen.height * 0.005f);
		}
		GUILayout.EndScrollView();
		GUILayout.EndArea();
		#endregion
		#region Items
		GUILayout.BeginArea(new Rect(Screen.width / 6, Screen.height / 35, Screen.width / 1.5f, Screen.height / 5f));
		itemsListScrollPos = GUILayout.BeginScrollView(itemsListScrollPos, true, false);
		GUILayout.BeginHorizontal();
		int counter = 0;
		foreach (var item in displayedItems)
		{
			if (GUILayout.Button(item.ItemName,
				item == selectedItem ? skin.GetStyle("selectedbutton") : skin.GetStyle("Button"),
				GUILayout.MinWidth(Screen.width * 0.1f), GUILayout.MinHeight(Screen.height * 0.17f)))
			{
				selectedItem = item;
			}
			counter++;
			GUILayout.Space(Screen.width * 0.005f);
		}
		GUILayout.EndHorizontal();
		GUILayout.EndScrollView();
		GUILayout.EndArea();
		if (selectedItem != null)
		{

			GUI.Box(new Rect(Screen.width / 6, Screen.height / 4, Screen.width / 1.5f, Screen.height / 1.85f),
				selectedItem.ItemName + "\n" + selectedItem.FormattedStats());
		}
		if (selectedItem != null)
		{
			GUI.enabled = selectedItem.GetType() == typeof (Armour) || selectedItem.GetType() == typeof (Weapon);
		}
		else
		{
			GUI.enabled = false;
		}
		if (GUI.Button(new Rect(Screen.width / 2.5f, Screen.height / 1.25f, Screen.width / 4, Screen.height / 20), "Equip"))
				{
					Inventory.InventoryItems.Remove(selectedItem);
					Inventory.Equip(selectedItem);
					UpdateItems();
				}

		#endregion
	}
}
