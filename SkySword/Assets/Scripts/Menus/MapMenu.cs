﻿using System;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;
using System.Collections;

public class MapMenu : Menu
{
	public Camera fullMapCam;
	public float mapZoomSpeed;
	public float mapDragSpeed = 50f;
	public GUISkin skin;
	public Texture2D shopSymbolTexture;
	public Texture2D questGiverAvailableTexture;
	public Texture2D questGiverCompleteTexture;
	public QuestGiver[] questGiversCached;

	void Start()
	{
		StartCoroutine("TryFindMapCam");
	}

	void OnEnable()
	{
		questGiversCached = FindObjectsOfType<QuestGiver>();
		if (fullMapCam)
		{
			fullMapCam.enabled = true;
			fullMapCam.depth = 1;
		}
	}

	void OnDisable()
	{
		if (fullMapCam)
		{
			fullMapCam.enabled = false;
		}
	}

	private void Update()
	{
		if (fullMapCam)
		{
			fullMapCam.orthographicSize += -Input.GetAxis(InputNames.ZoomMap) * mapZoomSpeed;
			if (Input.GetButton(InputNames.PrimaryAttack))
			{
				fullMapCam.transform.Translate(-Input.GetAxis(InputNames.LookHorizontal) * mapDragSpeed,
					-Input.GetAxis(InputNames.LookVertical) * mapDragSpeed,
					0,
					Space.Self);
			}
			if (!fullMapCam.enabled)
			{
				fullMapCam.depth = 1;
				fullMapCam.enabled = true;
			}
		}
	}

	private void OnGUI()
	{
		GUI.skin = skin;
		foreach (var quest in CommonPlayerData.quests)
		{
			foreach (var objective in quest.questObjectives)
			{
				if (!objective.isCompleted && objective.isVisible)
				{
					var screenPos = fullMapCam.WorldToScreenPoint(objective.worldPositon);
					if (screenPos.x > 0 && screenPos.y > 0)
					{
						float x = screenPos.x - (Screen.width * 0.015f),
							y = Screen.height - (screenPos.y - (Screen.width * 0.015f));
						var rect = new Rect(x, y,
							Screen.width * 0.03f, Screen.width * 0.03f);
						if (rect.Contains(Event.current.mousePosition))
						{
							GUI.tooltip = objective.ObjectiveString();
						}
						GUI.Box(rect, quest.questObjectives.IndexOf(objective).ToString(CultureInfo.InvariantCulture),
							skin.GetStyle("QuestObjectiveSymbol"));
					}
				}
			}
		}
		foreach (var shop in GameDataManager.shops)
		{
			var screenPos = fullMapCam.WorldToScreenPoint(shop.Value.worldPos);
			if (screenPos.x > 0 && screenPos.y > 0)
			{
				float x = screenPos.x - (Screen.width * 0.015f),
					y = Screen.height - (screenPos.y - (Screen.width * 0.015f));
				var rect = new Rect(x, y, Screen.width * 0.03f, Screen.width * 0.03f);
				if (rect.Contains(Event.current.mousePosition))
				{
					GUI.tooltip = shop.Key;
				}
				GUI.DrawTexture(rect, shopSymbolTexture);
			}
		}
		foreach (var questGiver in questGiversCached)
		{
			var screenPos = fullMapCam.WorldToScreenPoint(questGiver.transform.position);
			if (screenPos.x > 0 && screenPos.y > 0)
			{
				float x = screenPos.x - (Screen.width * 0.015f),
					y = Screen.height - (screenPos.y - (Screen.width * 0.015f));
				var rect = new Rect(x, y, Screen.width * 0.03f, Screen.width * 0.03f);
				if (rect.Contains(Event.current.mousePosition))
				{
					var availableQuestString = new StringBuilder();
					var completedQuestString = new StringBuilder();
					foreach (var quest in questGiver.quests)
					{
						if (!quest.CheckIfCompleted() && quest.questStatus != QuestStatus.Accepted)
						{
							availableQuestString.Append("\n" + quest.questName);
						}
						else
						{
							completedQuestString.Append("\n" + quest.questName);
						}
					}

					GUI.tooltip = questGiver.Name + (availableQuestString.Length > 0 ? "\nAvailable Quests:" : "") +
					              availableQuestString + (completedQuestString.Length > 0 ? "\nCompletedQuests:" : "") +
					              completedQuestString;
				}
				if (questGiver.quests.Any(quest => quest.CheckIfCompleted()))
				{
					GUI.DrawTexture(rect, questGiverCompleteTexture);

				}
				else
				{

					GUI.DrawTexture(rect, questGiverAvailableTexture);
				}
			}
		}
		if (GUI.tooltip != "")
		{
			var mousePos = Event.current.mousePosition;
			GUI.Box(new Rect(mousePos.x, mousePos.y + 15, Screen.width * 0.25f, Screen.height * 0.1f),
				GUI.tooltip);
		}
	}

	IEnumerator TryFindMapCam()
	{
		while (!fullMapCam)
		{
			var fullMapCamObject = GameObject.Find("FullMapCam");
			if (fullMapCamObject)
			{
				fullMapCam = fullMapCamObject.GetComponent<Camera>();

			}
			yield return null;
		}
		fullMapCam.enabled = false;
	}
}
