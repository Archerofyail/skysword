﻿using System.Collections.Generic;
using UnityEngine;

public class ViewQuestMenu : Menu
{
	public GUISkin guiSkin;
	public List<Quest> quests;
	private int selectedQuestIndex = -1;
	private Vector2 questScrollViewPos;
	void Start()
	{

	}

	void Update()
	{

	}

	void OnGUI()
	{
		GUI.skin = guiSkin;
		GUILayout.BeginArea(new Rect(Screen.width * 0.05f, Screen.height * 0.05f, Screen.width * 0.22f, Screen.height * 0.85f));
		questScrollViewPos = GUILayout.BeginScrollView(questScrollViewPos, false, true, GUIStyle.none,
			guiSkin.GetStyle("verticalscrollbar"), GUILayout.MinHeight(Screen.height * 0.85f));
		if (quests != null)
		{
			if (quests.Count > 0)
			{
				bool areAnyQuestsVisible = false;
				int count = 0;
				foreach (var quest in quests)
				{
					quest.CheckIfCompleted();
					if (quest.isAvailable)
					{
						areAnyQuestsVisible = true;
						if (GUILayout.Button(quest.questName,
							count == selectedQuestIndex ? guiSkin.GetStyle("selectedbutton") : guiSkin.GetStyle("Button"),
							GUILayout.MinWidth(Screen.width * 0.2f)))
						{
							selectedQuestIndex = count;
						}
					}
					count++;
					GUILayout.Space(Screen.height * 0.01f);
				}
				if (!areAnyQuestsVisible)
				{
					GUILayout.Label("No Quests Available", GUILayout.MinWidth(Screen.width * 0.2f));
				}
			}
			else
			{
				GUILayout.Label("No Quests Available", GUILayout.MinWidth(Screen.width * 0.2f));
			}
		}
		else
		{
			GUILayout.Label("No Quests Available", GUILayout.MinWidth(Screen.width * 0.2f));
		}

		GUILayout.EndScrollView();
		GUILayout.EndArea();
		if (selectedQuestIndex > -1)
		{
			GUILayout.BeginArea(new Rect(Screen.width * 0.3f, Screen.height * 0.05f, Screen.width * 0.6f, Screen.height * 0.85f));
			GUILayout.Box(quests[selectedQuestIndex].questName, GUILayout.MinHeight(50));
			GUILayout.Box(quests[selectedQuestIndex].questDescription, GUILayout.MinHeight(150));
			//GUILayout.Box(quests[selectedQuestIndex].objectiveDescription, );
			GUILayout.BeginVertical(guiSkin.GetStyle("Box"), GUILayout.MinHeight(100));

			foreach (var questObjective in quests[selectedQuestIndex].questObjectives)
			{
				if (questObjective.isVisible)
				{
					GUILayout.Toggle(questObjective.isCompleted, questObjective.ObjectiveString());
				}
			}
			GUI.enabled = true;
			GUILayout.EndVertical();
			GUI.enabled = quests[selectedQuestIndex].CheckIfCompleted() || quests[selectedQuestIndex].questStatus == QuestStatus.NotAccepted;
			if (
				GUILayout.Button(quests[selectedQuestIndex].questStatus != QuestStatus.NotAccepted
					? "Complete Quest"
					: "Accept Quest", GUILayout.ExpandWidth(true)))
			{
				if (quests[selectedQuestIndex].questStatus == QuestStatus.NotAccepted)
				{
					print("Selected Accept Quest Button");
					quests[selectedQuestIndex].questStatus = QuestStatus.Accepted;
					CommonPlayerData.quests.Add(quests[selectedQuestIndex]);
				}
				else if (quests[selectedQuestIndex].questStatus == QuestStatus.Completed)
				{
					print("Selected Complete Quest Button");
					CommonPlayerData.quests.Remove(quests[selectedQuestIndex]);
					quests[selectedQuestIndex].isAvailable = false;
					selectedQuestIndex = -1;
				}
			}
			GUI.enabled = true;

			GUILayout.EndArea();
		}
		else
		{
			GUILayout.BeginArea(new Rect(Screen.width * 0.3f, Screen.height * 0.05f, Screen.width * 0.6f, Screen.height * 0.85f));
			GUILayout.Box("", GUILayout.MinHeight(50));
			GUILayout.Box("", GUILayout.MinHeight(150));
			GUILayout.Box("", GUILayout.MinHeight(100));
			GUILayout.EndArea();
		}
	}
}
