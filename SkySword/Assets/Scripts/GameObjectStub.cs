﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A script that handles replacing the menu manager
/// </summary>
public class GameObjectStub : MonoBehaviour
{
	/// <summary>
	/// The name of the object this one is replacing
	/// </summary>
	public string objectName = "MenuManager";

	void Awake()
	{
		//looks for the real object, if it's found destroy the this one
		var tempObj = GameObject.Find(objectName);
		if (tempObj)
		{
			MenuManager.SetMenuManagerObject(tempObj);
			Destroy(gameObject);
		}
	}

	void Start()
	{
		//Sets the menu manager object to be this one
		MenuManager.SetMenuManagerObject(gameObject);
	}

	void Update()
	{
		//Checks every update for the real object, if it's found destroy this one
		var tempObj = GameObject.Find(objectName);
		if (tempObj)
		{
			MenuManager.SetMenuManagerObject(tempObj);
			Destroy(gameObject);
		}
	}
}
