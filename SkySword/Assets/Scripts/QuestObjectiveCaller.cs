﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

/// <summary>
/// The class that will call an event for quest objectives that subscribe to it using the objectiveName string
/// </summary>
public class QuestObjectiveCaller : MonoBehaviour
{
	private bool wasKeyPressed;
	public delegate void ObjectiveReachedEvent(QuestObjectiveCaller caller, string sender);
	public event ObjectiveReachedEvent ObjectiveReachedEventHandler;
	public string objectiveName = "Default";
	private Texture2D questTargetTexture; 

	private void OnDestroy()
	{

		if (ObjectiveReachedEventHandler != null)
		{
			ObjectiveReachedEventHandler(this, ObjectiveEventNames.ObjectDestroyed);
		}
	}

	void Update()
	{
		if (wasKeyPressed)
		{
			ObjectiveReachedEventHandler(this, Input.inputString ?? "");
			wasKeyPressed = false;
		}
		if (Input.anyKeyDown && ObjectiveReachedEventHandler != null)
		{
			wasKeyPressed = true;
		}
	}

	void OnGUI()
	{
		
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == Tags.Player && ObjectiveReachedEventHandler != null)
		{
			ObjectiveReachedEventHandler(this, ObjectiveEventNames.PlayerTrigger);
		}
	}
}
